﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(ItemDatabase))]
public class ItemDatabaseEditor : Editor
{
    /* SerializedProperty itemProp;

     private void Awake()
     {

     }
     public override void OnInspectorGUI()
     {
         serializedObject.Update();
         ItemDatabase database = (ItemDatabase)target; 

         itemProp = serializedObject.FindProperty("gameItems");
         EditorGUILayout.PropertyField(itemProp,true);

       for (int i = 0; i < itemProp.arraySize; i++)
         {
             if (database.gameItems[i]!=null)
             {
                 EditorGUILayout.TextField(database.gameItems[i].noun);

             }
         }
         //EditorGUILayout.PropertyField(itemProp.FindPropertyRelative("value"),true);
         serializedObject.ApplyModifiedProperties();
     }*/
    public override void OnInspectorGUI()
    {
        ItemDatabase itemDatabase = (ItemDatabase)target;
        if(GUILayout.Button("Register"))
        {
            itemDatabase.CreateDatabase();
        }
        base.OnInspectorGUI();
        Rect position = new Rect(0, 0, 100, 100);
        SerializedProperty database = serializedObject.FindProperty("database");
        //EditorGUILayout.PropertyField(position, database)
        for (int i = 0; i < database.FindPropertyRelative("items").arraySize; i++)
        {
            Rect pos=EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(database.FindPropertyRelative("items").GetArrayElementAtIndex(i),GUIContent.none);
            EditorGUILayout.PropertyField(database.FindPropertyRelative("value1").GetArrayElementAtIndex(i), GUIContent.none);
            EditorGUILayout.PropertyField(database.FindPropertyRelative("value2").GetArrayElementAtIndex(i), GUIContent.none);
            EditorGUILayout.PropertyField(database.FindPropertyRelative("value3").GetArrayElementAtIndex(i), GUIContent.none);
            EditorGUILayout.EndHorizontal();
        }


    }
}
