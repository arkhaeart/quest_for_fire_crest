﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ItemEditor : MonoBehaviour
{

}
[CustomEditor(typeof(Consumable))]
public class ConsEditor: Editor
{
    Consumable cons
    {
        get
        {
            return target as Consumable;
        }
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        for (int i = 0; i < cons.effects.Length; i++)
        {
            if(cons.effects[i].param==ConsumeParam.SKILL||cons.effects[i].param==ConsumeParam.STAT)
            {
                cons.effects[i].subparam = EditorGUILayout.TextField("SubParameter", cons.effects[i].subparam);
            }
        }
        
    }
}
