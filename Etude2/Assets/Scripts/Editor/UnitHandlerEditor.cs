﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(UnitHandlerScript))]
public class UnitHandlerEditor : Editor
{
    private bool[] showUnitSLots;
    private SerializedProperty unitPropObject;
    private SerializedProperty unitPropDialog;
    private SerializedProperty unitPropStats;
    private SerializedProperty unitPropInv;
    private SerializedProperty unitPropPatrolRoute;
    private SerializedProperty subPropWayPoints;
    private const string unitPropObjectName = "unitList";
    private const string unitPropDialogName = "dialogList";
    private const string unitPropStatsName = "statsList";
    private const string unitPropInvName = "invList";
    private const string unitPropPatrolName = "wayList";
    private const string subPropWayName = "wayTPoints";

    private UnitHandlerScript script
    {
        get
        {
            return target as UnitHandlerScript;
        }
    }
    private void Awake()
    {
        
    }
    private void OnEnable()
    {
        unitPropObject = serializedObject.FindProperty(unitPropObjectName);
        unitPropDialog = serializedObject.FindProperty(unitPropDialogName);
        unitPropStats = serializedObject.FindProperty(unitPropStatsName);
        unitPropInv = serializedObject.FindProperty(unitPropInvName);
        unitPropPatrolRoute = serializedObject.FindProperty(unitPropPatrolName);

        showUnitSLots = new bool[script.unitList.Count];
    }
    public override void OnInspectorGUI()
    {
        
        
        script.unitHandler = (UnitHandler)EditorGUILayout.ObjectField(script.unitHandler,typeof(UnitHandler),false);
        script.ImplementOnRun = EditorGUILayout.Toggle(this.script.ImplementOnRun);
        
 
        
        serializedObject.Update();

        
        for (int i = 0; i < script.unitList.Count; i++)
        {
            UnitSlotGUI(i, script.unitList.Count);
        }
        serializedObject.ApplyModifiedProperties();
        
        if (GUILayout.Button("Register All"))
        {
            script.RegisterAll();
        }
        if(GUILayout.Button("SaveChanges"))
        {
            script.SaveChanges();
        }
        if (GUILayout.Button("Implement"))
        {
            script.ImplementChanges();
        }

    }
    void UnitSlotGUI(int index, int count)
    {
        UnitHandlerScript unitHandlerScript = (UnitHandlerScript)target;
        //showUnitSLots[index] = false;
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.indentLevel++;
        showUnitSLots[index]=EditorGUILayout.Foldout(showUnitSLots[index], index+ " "+unitHandlerScript.unitList[index].name+" "+ unitHandlerScript.unitList[index].tag.ToString());
       
        if (showUnitSLots[index])
        {
            SerializedObject unit =new SerializedObject( unitHandlerScript.unitList[index]);
            unit.Update();
            EditorGUILayout.PropertyField(unitPropObject.GetArrayElementAtIndex(index));
            EditorGUILayout.PropertyField(unit.FindProperty("testDialog"));
            EditorGUILayout.PropertyField(unit.FindProperty("RPGStats"));
            EditorGUILayout.PropertyField(unit.FindProperty("inventoryList"));
            SerializedProperty patrolFolder = unitPropPatrolRoute.GetArrayElementAtIndex(index);
            subPropWayPoints = patrolFolder.FindPropertyRelative(subPropWayName);
            EditorGUI.indentLevel++;
            //EditorGUILayout.PropertyField(unitPropPatrolRoute.GetArrayElementAtIndex(index));
            EditorGUILayout.PropertyField(subPropWayPoints,true);
            EditorGUI.indentLevel--;
            unit.ApplyModifiedProperties();
        }
        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
        
    }
    
}
