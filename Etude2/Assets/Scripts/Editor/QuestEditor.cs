﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(Quest))]
public class QuestEditor : Editor
{
    SerializedProperty affs;

    Quest quest

    {
        get
        {
            return target as Quest;
        }
    }
    public override void OnInspectorGUI()
    {
        
        DrawDefaultInspector();
        serializedObject.Update();
        affs = serializedObject.FindProperty("affiliates");
        if (GUILayout.Button("AddOne"))
            quest.AddAff();
        Debug.Log(affs);
        if (affs.arraySize > 0)
        {
            for (int i = 0; i < affs.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(affs.GetArrayElementAtIndex(i));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("affTypes").GetArrayElementAtIndex(i));
                EditorGUILayout.EndHorizontal();
            }
        }
        serializedObject.ApplyModifiedProperties();
    }
}
/*[CustomPropertyDrawer(typeof(Quest.Condition))]
public class QuestConditionEditor : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //base.OnGUI(position, property, label);
        Debug.Log("hello");
    }

}*/
