﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(PatrolSystem))]

public class PatrolEditor : Editor
{
    PatrolSystem instance;
    bool[] unitSlots;
    private SerializedProperty unitsProperty;
    private SerializedProperty patrolWaysProperty;
    private const string unitPropName = "units";
    private const string patrolWayPropName = "patrolWays";
    private void Awake()
    {
       
    }
    private void OnEnable()
    {
        unitsProperty = serializedObject.FindProperty(unitPropName);
        patrolWaysProperty = serializedObject.FindProperty(patrolWayPropName);
        instance = (PatrolSystem)target;
        unitSlots = new bool[instance.units.Length];
        
    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update();
        for (int i = 0; i < instance.units.Length; i++)
        {
            GUISlot(i);
        }
    }
    void GUISlot(int index)
    {
        
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.indentLevel++;
        unitSlots[index] = EditorGUILayout.Foldout(unitSlots[index], index +" unit.");

        if (unitSlots[index])
        {
            EditorGUILayout.PropertyField(unitsProperty.GetArrayElementAtIndex(index));
            EditorGUILayout.PropertyField(patrolWaysProperty.GetArrayElementAtIndex(index));

        }
        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();
    }
}
*/