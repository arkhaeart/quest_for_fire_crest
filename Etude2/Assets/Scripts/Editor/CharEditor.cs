﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(Char),true)]
public class CharEditor : Editor
{
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        serializedObject.Update();
        Char player = (Char)target;
        if (player.rpg.armorDict != null&& player.rpg!=null)
        {
            for (int i = 0; i < player.rpg.armorDict.Count; i++)
            {

                EditorGUI.indentLevel++;
                if (player.rpg.armorDict[ItemDatabase.slotDict[i]] != null)
                    EditorGUILayout.LabelField(player.rpg.armorDict[ItemDatabase.slotDict[i]].noun);
                EditorGUI.indentLevel--;

            }
        }
    }
}
