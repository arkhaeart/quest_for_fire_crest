﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class MovingUnit : MonoBehaviour
{
    Animator animator;
    public BaseASM animScript;
    public Char character;
    public Transform currentGoal;

    Rigidbody2D rb;
    float speed=1f;
    Vector3[] path;
    int targetIndex;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
            
        animator = GetComponent<Animator>();
        animScript = animator.GetBehaviour<BaseASM>();
        character = GetComponent<Char>();
        animScript.character = character;
        animScript.animator = animator;
    }
    private void Start()
    {


        Patrol();
  
    }
    public void Patrol()
    {
        PatrolSystem.GetNextWaypoint(this, OnReceivePatrolPoint);
    }
    public void MoveToPoint(Transform goal)
    {
        PathRequestManager.RequestPath(transform.position, goal.position, OnPathFound);

    }
    void OnReceivePatrolPoint(Vector3 point, bool success)
    {
        if(success)
            PathRequestManager.RequestPath(transform.position, point, OnPathFound);
    }
    public void OnPathFound(Vector3[] newpath, bool success)
    {
        if (success)
        {
            path = newpath;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }
    IEnumerator FollowPath()
    {
        animator.enabled = true;
        targetIndex = 0;
        Vector3 currentWaypoint = path[0];
        animScript.MoveDirection(currentWaypoint - transform.position);
        while (true)
        {
            if(transform.position==currentWaypoint)
            {
                targetIndex++;
                if(targetIndex>=path.Length)
                {
                    Invoke("Patrol", 3f);
                    animScript.StopMoving();
                    yield break;
                }
                currentWaypoint = path[targetIndex];
                animScript.MoveDirection( currentWaypoint - transform.position);
            }
            
            
            Vector3 newPos = Vector3.MoveTowards(transform.position, currentWaypoint, speed*Time.deltaTime);
            rb.MovePosition(newPos);
            yield return null;
        }
    }

    public void OnDrawGizmos()
    {
        if(path!=null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one * 0.03f);
                if (i == targetIndex)
                    Gizmos.DrawLine(transform.position, path[i]);
                else
                    Gizmos.DrawLine(path[i - 1], path[i]);
            }
        }
    }
}
