﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class MovDirections : MonoBehaviour
{
    public static string moveDirsFileName = "moveDirs.json";
    public static int[,] directions;
    private void Awake()
    {
        //LoadMoveDirs();
    }
    public static void SaveMoveDirs()   
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, moveDirsFileName);
        
        
        MovDirs raw = new MovDirs();
        for (int i = 0; i < 3; i++)
        {
            raw.first_raw[i] = 0;
            raw.second_raw[i] = 0;
            raw.third_raw[i] = 0;
        }
        string jsData=JsonUtility.ToJson(raw);
        File.WriteAllText(filePath, jsData); 
    }
    public static int[,] GetDirections
    {
        get
        {
            return LoadMoveDirs();
        }
    }
    public static int[,] LoadMoveDirs()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, moveDirsFileName);
        string jsData = File.ReadAllText(filePath);
        MovDirs raw = JsonUtility.FromJson<MovDirs>(jsData);
        directions = new int[3, 3];
        for (int i = 0; i < 3; i++)
        {
            directions[0, i] = raw.first_raw[i];
            directions[1, i] = raw.second_raw[i];
            directions[2, i] = raw.third_raw[i];
        }
        return directions;
    }
}
[System.Serializable]
public class MovDirs
{
    public int[] first_raw;
    public int[] second_raw;
    public int[] third_raw;
    public MovDirs()
    {
        first_raw = new int[3];
        second_raw = new int[3];
        third_raw = new int[3];
    }
}
