﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GRID : MonoBehaviour
{
    public bool displayGrid;
    //public Transform player;
    public LayerMask unWalkable;
    public Node[,] grid;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    public List<Node> path;
    [HideInInspector] public Vector3 worldBottomLeft;

    float nodeDiameter;
    int gridSizeX, gridSizeY;
    // Start is called before the first frame update
    void Awake()
    {
        //player = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Legs>().transform;
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }
    void CreateGrid()
    {
        int number = 0;
        grid = new Node[gridSizeX, gridSizeY];
        worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.up * gridWorldSize.y/2;
        
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y= 0; y < gridSizeY; y++)
            {
                Vector3 worldRosition = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics2D.OverlapCircle(worldRosition, nodeRadius,unWalkable));
                grid[x, y] = new Node(walkable, worldRosition,x,y);

                number++;
            }
        }
        
    }
    public int MaxSize
    {
        get
        {
            return gridSizeX * gridSizeY;
        }
    }
    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y  = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;
                int checkX = node.gridX + x;
                int checkY = node.gridY + y;
                if(checkX>=0&&checkX<gridSizeX&&checkY>=0&&checkY<gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }



            }
        }
        
            
        return neighbours;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public Node NodeFromWorldPos(Vector2 position)
    {
        
        float percentX = (position.x - worldBottomLeft.x)/ gridWorldSize.x;
        float percentY = (position.y- worldBottomLeft.z)/ gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);
        int x = Mathf.RoundToInt((gridSizeX) * percentX)-1;
        int y = Mathf.RoundToInt((gridSizeY) * percentY)-1;
        return grid[x, y];
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector2(gridWorldSize.x, gridWorldSize.y));
        
        
        
            if (grid != null&&displayGrid)
            {
                foreach (Node n in grid)
                {
                    Gizmos.color = (n.walkable) ? Color.white : Color.red;   
                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - 0.03f));
                }
            }
        
    }
}
