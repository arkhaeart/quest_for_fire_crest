﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Entry
{
    public string entryID;
    [TextArea]
    public string entryText;
    public DialogAnswers[] dialogAnswers;
    
}
[System.Serializable]
public class DialogAnswers
{
    public string answerID;
    public string prereq;
    [TextArea]
    public string answerText="defaultText";
    public string actionResponse="default";
    public DialogExit[] dialogExits;
    public DialogAnswers()
    {
        answerID = "defaultID";
        answerText = "defaultText";
    }
}
[System.Serializable]
public class DialogExit
{
    public string exitID;
    public string requirements;
    public string entryToGoTo;
}
[System.Serializable]

public class DialogActions
{
    public string ActionID;
    public enum Actions
    {
        END,
        FIGHT,
        MONEY,
        ITEM,
        TRIGGER,
        DEFAULT
    }
    public Actions action;
    public float number;
    public string item;

}

[System.Serializable]
public class BattleDialogActions
{
    public string ActionID;
    public BattleActions battleActions;
}
