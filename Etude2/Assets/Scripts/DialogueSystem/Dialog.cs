﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="FireCrest/Dialog")]

public class Dialog : ScriptableObject
{
    
    public Entry[] entries;
    public DialogActions[] dialogActions;
    public Entry StartingEntry
    {
        get
        {
            foreach (Entry en in entries)
            {
                if (en.entryID == "default")
                    return en;
            }
            throw new System.NotImplementedException();
        }
    }
}
