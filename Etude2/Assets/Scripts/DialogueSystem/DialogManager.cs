﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour
{
    public GameObject player;
    public GameObject speaker;
    public GameObject[] answerButton;
    Dialog currentDialog;
    public Entry currentEntry;
    public Text entryText;
    public Text[] answerText;
    public GameObject answerPanel;
    public BattleManager battleManager;
    public QuestSystem questSystem;
    
    protected DialogAnswers currentAnswer;

    // Start is called before the first frame update
    protected virtual void Awake()
    {


        for (int i = 0; i < answerButton.Length; i++)
        {
            answerButton[i].GetComponent<AnswerButton>().buttonNumber = i;
           
        }
    }
    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void InitDialog(GameObject speakerObj)
    {
        speaker = speakerObj;
       
        currentDialog = speakerObj.GetComponent<Char>().testDialog;
        if (currentDialog == null)
            return;
        gameObject.SetActive(true);
        currentEntry = currentDialog.StartingEntry;
        UnpackEntry();
    }
    public void UnpackEntry()
    {
        ClearDialogWindow();
        entryText.text = currentEntry.entryText;
        int i = 0;
        foreach (DialogAnswers answer in currentEntry.dialogAnswers)
        {
            if (!string.IsNullOrEmpty(answer.prereq))
            {
                if (!CheckPrereq(answer.prereq))
                {
                    continue;
                }
            }
                answerText[i].text = answer.answerText;
                answerButton[i].GetComponent<AnswerButton>().answerID = answer.answerID;
                answerButton[i].SetActive(true);
                i++;
            
        }
    }
    bool CheckPrereq(string prereq)
    {

        string[] req = prereq.Split('_');
        if (req[0] == "quest")
        {
            Debug.Log(questSystem);
            return questSystem.CheckPrereq(req);
        }
        else
            return true;
    }
    public void ClearDialogWindow()
    {
        entryText.text = null;
        for (int i = 0; i < answerButton.Length; i++)
        {
            answerText[i].text = null;
            answerButton[i].SetActive(false);
        }
    }
    public virtual void OnGetButtonResponse(string answerID)
    {
        DialogActions currentAction=new DialogActions();
        currentAction.action = DialogActions.Actions.DEFAULT;
        if (answerID=="end")
        {
            FinishConversation();
            return;
        }
        

        for (int i = 0; i < currentEntry.dialogAnswers.Length; i++)
        {
            if (currentEntry.dialogAnswers[i].answerID == answerID)
            {
                currentAnswer = currentEntry.dialogAnswers[i];
                break;
            }

        }
        if (currentAnswer.actionResponse != null)
        {
            Debug.Log(currentAnswer.actionResponse);
            for (int i = 0; i < currentDialog.dialogActions.Length; i++)
            {
                if (currentDialog.dialogActions[i].ActionID == currentAnswer.actionResponse)
                {
                    currentAction = currentDialog.dialogActions[i];
                }
            }

            switch (currentAction.action)
            {
                case (DialogActions.Actions.END):
                    FinishConversation();
                    return;
                case (DialogActions.Actions.FIGHT):
                    FinishConversation();
                    Char[] combatants = new Char[2];
                    /*for (int i = 0; i < combatants.Length; i++)
                    {
                        combatants[i]
                    }*/
                    combatants[0] = player.GetComponent<Char>();
                    combatants[1] = speaker.GetComponent<Char>();
                    battleManager.InitBattle(combatants);
                    return;
                case (DialogActions.Actions.TRIGGER):
                    if (currentAction.ActionID.StartsWith("quest_start"))
                    {
                        questSystem.QuestAwake(currentAction.item);
                    }
                    else if (currentAction.ActionID.StartsWith("quest_react"))
                    {
                        questSystem.QuestReaction(currentAction.item, (int)currentAction.number);
                    }
                    break;

            }
        }
        int currentExitNumber = 0; //now primitivized, to be changed
        for (int i = 0; i < currentDialog.entries.Length; i++)
        {
            if (currentAnswer.dialogExits[currentExitNumber].entryToGoTo == currentDialog.entries[i].entryID)
            {
                currentEntry = currentDialog.entries[i];
                UnpackEntry();
                return;
            }
        }

    

    }
    public void FinishConversation()
    {
        ClearDialogWindow();
        currentDialog = null;
        currentEntry = null;
        gameObject.SetActive(false);

    }
}
