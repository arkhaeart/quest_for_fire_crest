﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour
{
    public int buttonNumber;
    public string answerID;
    public GameObject dialogManagerObject;
    public DialogManager dialogManager;

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        dialogManager = dialogManagerObject.GetComponent<DialogManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnButtonClick()
    {
        
        dialogManager.OnGetButtonResponse(answerID);

    }
}
