﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container:Char
{

protected override void Start()
    {
        rpg = new RPG();
        if (inventoryList != null)
            rpg.inventory = Inventory.InitInv(inventoryList);
        else
            rpg.inventory = Inventory.InitInv();
    }
}
