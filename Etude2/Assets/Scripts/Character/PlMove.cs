﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlMove : MonoBehaviour
{

    public float speed = 1.5f;
    public int movedir;
    public BaseASM animScript;

    Rigidbody2D PlRb;
    Animator anim;
    Char character;
   
    // Start is called before the first frame update
    void Start()
    {
        PlRb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        character = GetComponent<Char>();
        animScript = anim.GetBehaviour<BaseASM>();
        animScript.character = character;
        animScript.animator = anim;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        if (h == 0 && v == 0)
        {
            animScript.StopMoving();
        }
        else
            Move(h, v);

    }
    void Move(float h, float v)
    {
        Vector3 movement = new Vector3(h, v,0f).normalized*speed*Time.deltaTime;
        PlRb.MovePosition(transform.position+ movement);
        
        Vector3 animove = new Vector3(h, v, 0f);
        animScript.MoveDirection(animove);
    }
  
        

}
