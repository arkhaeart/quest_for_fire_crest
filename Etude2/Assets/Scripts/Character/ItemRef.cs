﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRef : FCObject, InteractableUnit      
{
    public Item itemRef;
    public Collider2D coll;
    void Awake()
    {
        coll = GetComponent<CircleCollider2D>();
        questReference = itemRef.noun;
    }

}
