﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChar : Char
{

    public PlMove plMove;
    public List<string> interactableTags;
    GameObject lastInteractable;
    protected override void Awake()
    {
        base.Awake();

        
        
    }
    protected override void Start()
    {
        base.Start();
        plMove = GetComponent<PlMove>();
        questReference = "player";
    }
    public override void Death()
    {
        base.Death();
        plMove.enabled = false;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (interactableTags.Contains(collision.tag))
        {
            GameManager.hub.InitInteraction(collision.gameObject);
            lastInteractable = collision.gameObject;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Container"))
            GameManager.hub.CheckContainer(collision.gameObject);
        if (collision.gameObject==lastInteractable)
            GameManager.hub.NoInteraction();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
