﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(Rigidbody2D))]

public class Char : FCObject,InteractableUnit
{
    public string unName;
    public Rigidbody2D charRb;
    public Animator charAnim;
    
    public Collider2D trig;


    public RPG rpg;


    public RPGStatsMain RPGStats;
    public Dialog testDialog;
    public InventoryList inventoryList;
    public int unitRegNumber;
    BaseASM asm;

    
    protected virtual void Awake()
    {
       


    }
    protected virtual void Start()
    {
        InitPlayer();
    }


    protected void Registration(GameObject gameManag)
    {

        //UnitHandlerScript unitHandlerScript = gameManag.GetComponent<UnitHandlerScript>();
        // unitHandlerScript.RegisterUnit(gameObject);
    }

    public void InitPlayer()
    {

        rpg = new RPG(RPGStats);
        rpg.armorDict = ItemDatabase.InitArmor();
        if (inventoryList != null)
            rpg.inventory = Inventory.InitInv(inventoryList);
        else
            rpg.inventory = Inventory.InitInv();

        trig = GetComponent<Collider2D>();
        charRb = GetComponent<Rigidbody2D>();
        charAnim = GetComponent<Animator>();
        asm = charAnim.GetBehaviour<BaseASM>();
        asm.MoveDir = 1;
        charAnim.SetBool("IsMoving", false);
        rpg.parent = this;
    }



    public void ProcessBattleAnimations(AnimStates state)
    {
        switch (state)
        {
            case (AnimStates.BATTLE):
                asm.InitBattle();
                break;
            case (AnimStates.ATTACK):
                asm.Attack();
                break;
            case (AnimStates.BEINGHIT):
                asm.BeenHit();
                break;
            case (AnimStates.DEAD):
                asm.Dead();
                break;
        }
    }
    public void Stop()
    {
        asm.StopAnimation();
    }
    public void OnAttack()
    {
        BattleManager.finishedHit = true;
    }
    public virtual void Death()
    {
        Debug.Log(gameObject.name + " is dead.");
        GameManager.quest.QuestObjectCheck(questReference, Quests.InteractionType.DESTROY);
      
    }
    public void InventoryToList()
    {
        inventoryList = ScriptableObject.CreateInstance<InventoryList>();
        inventoryList.inventory = rpg.inventory;
    }
}
public interface InteractableUnit
{
    void OnMouseDown();
}
