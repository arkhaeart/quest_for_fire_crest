﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(MovingUnit))]
public class NPC : Char
{
    public MovingUnit moving;
    public BattleAI battleAI;
    protected override void Awake()
    {
        base.Awake();
        moving = GetComponent<MovingUnit>();
    }
    protected override void Start()
    {
        base.Start();
        AutoEquip();
    }
    public void StopMovement()
    {
        moving.StopCoroutine("FollowPath");
        moving.animScript.StopMoving();
    }
    public override void Death()
    {
        base.Death();
        UnEquip("all");
        gameObject.tag = "Container";
    }
    public void UnEquip(string param)
    {
        if (param == "all")
        {
            InventoryList dropList = ScriptableObject.CreateInstance<InventoryList>();
            dropList.inventory = Equipment.RemoveWithReturnAll(rpg);
            rpg.inventory=Inventory.InitInv(dropList);
        }
    }
    public void AutoEquip()
    {
        for (int i = 0; i < rpg.inventory.Length; i++)
        {
            if (rpg.inventory[i] == null)
                continue;
            if (rpg.inventory[i].GetType() == typeof(Weapon))
            {
                if (rpg.weapon == null)
                    Equipment.AddItem(rpg, (Weapon)rpg.inventory[i]);
                else
                    continue; //some compare of weapon or random choose
            }
            else if (rpg.inventory[i].GetType() == typeof(Armor))
            {
                Armor armor = (Armor)rpg.inventory[i];
                    if (ItemDatabase.armorDict.ContainsKey(armor.wearingSlot))
                    {
                        if (rpg.armorDict[armor.wearingSlot] == null)
                        {
                            Equipment.AddItem(rpg,armor);
                        }
                    }
            }
            else
                continue;
            Debug.Log(rpg.inventory[i].noun + " equiped by " + gameObject.name);
            rpg.inventory[i] = null;

        }
    }
}
