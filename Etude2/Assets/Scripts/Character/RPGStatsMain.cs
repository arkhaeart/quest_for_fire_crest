﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "FireCrest/RPGStatsFile")]
public class RPGStatsMain : ScriptableObject
{
    public int baseDamage = 10;
    public int baseHitChance = 30;
    public RPGStats stats;
    public RPGSkills skills;
    public RPGValues values;
    public float CutDamage
    {
        get
        {
            return baseDamage * skills.onBlades/2f;
        }
    }
    public float BluntDamage;
    public float HitChance
    {
        get
        {
            float rawChance=baseHitChance * stats.onStat(stats.agility) / 2f;
            int index = RPGStats.Index(stats.agility);
            int lowerBorder = baseHitChance;
            switch(index)
            {
                case (0):
                    break;
                case (1):
                    lowerBorder = 45;
                    break;
                case (2):
                    lowerBorder = 75;
                    break;
                case (3):
                    lowerBorder = 100;
                    break;
            }
            return Mathf.Clamp(rawChance, lowerBorder, 150);
        }
    }
    public float ParryChance;
    public float DamageReduce
    {
        get
        {
            return Mathf.Log((stats.endurance - 32), 2)*10f;
        }
    }
}
