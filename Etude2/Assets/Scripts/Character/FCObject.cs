﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FCObject : MonoBehaviour
{
    public string questReference;
    public void OnMouseDown()
    {
        UIHub.ClickResponse(gameObject);

    }
    public void QuestTrigger(Quests.InteractionType type)
    {
        GameManager.quest.QuestObjectCheck(questReference,type);
    }
}
