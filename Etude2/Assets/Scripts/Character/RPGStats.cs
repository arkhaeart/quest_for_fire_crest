﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class RPG
{
    public Char parent;
    public RPGSkills skills;
    public RPGStats stats;
    public RPGValues values;
    public Weapon weapon;
    public Item[] inventory;
    public Dictionary<string, Armor> armorDict=new Dictionary<string, Armor>();
    public RPG (RPGStatsMain list)
    {

        skills = new RPGSkills(list);
        stats = new RPGStats(list);
        values = new RPGValues(list);
    }
    public RPG(RPG rpg)
    {
        weapon = ScriptableObject.CreateInstance<Weapon>();
        weapon = rpg.weapon;
        armorDict = ItemDatabase.armorDict;
        for (int i = 0; i < armorDict.Count; i++)
        {
            Armor armToAdd = ScriptableObject.CreateInstance<Armor>();
            armToAdd = rpg.armorDict[ItemDatabase.slotDict[i]];
            armorDict[ItemDatabase.slotDict[i]] = armToAdd;
        }
    }
    public RPG()
    {
        inventory = new Item[Inventory.maxInvSize];
    }
}
[System.Serializable]
public class RPGStats 
{
   [Range (0,100)] public int strenght, agility, endurance;
    public float onStat(int stat)
    {

            float mod = 0.02f;
            int index = Index(agility);
            switch(index)
            {
                case (0):
                    break;
                case (1):
                    mod = 0.025f;
                    break;
                    
                case (2):
                    mod = 0.03f;
                    break;
                case (3):
                    mod = 0.04f;
                    break;
            }
            return stat * mod;  
    }
    public float DamageReduce
    {
        get
        {
            return Mathf.Log((endurance - 32), 2) * 10f;
        }
    }
    public RPGStats(RPGStatsMain list)
    {
        strenght = list.stats.strenght;
        agility = list.stats.agility;
        endurance = list.stats.endurance;
    }
    public static int Index (float param)
    {
        return  param <= 15 ? 0 : param <= 45 ? 1 : param <= 75 ? 2 : 3;
    }
}

[System.Serializable]
public class RPGSkills
{
    [Range(0, 100)] public int blades, parrying;

    public RPGSkills(RPGStatsMain list)
    {
        blades = list.skills.blades;
        parrying = list.skills.parrying;
    }
    public float onBlades 
    {
        get
        {
            float mod = 0.1f;
            int index = RPGStats.Index(blades);
            switch (index)
            {
                case (0):
                    break;
                case (1):
                    mod = 0.06f;
                    break;
                case (2):
                    mod = 0.055f;
                    break;
                case (3):
                    mod = 0.05f;
                    break;
                    
            }
            return blades * mod;
        }
    }
}

[System.Serializable]
public class RPGValues
{
    public int maxHealth;
    public int curHealth;
    public int maxMagic;
    public int curMagic;
    public int maxStamina;
    public int curStamina;
    public int experience;
    public int Level
    {
        get
        {
            return experience / 1000;
        }
    }
    public RPGValues()
    {
        maxHealth = 100;
        curHealth = maxHealth;
        maxMagic = 50;
        curMagic = maxMagic;
        maxStamina = 100;
        curStamina = maxStamina;
        experience = 0;
        
    }
    public RPGValues(RPGStatsMain list)
    {
        maxHealth = list.values.maxHealth;
        curHealth = maxHealth;
        maxMagic = list.values.maxMagic;
        curMagic = maxMagic;
        maxStamina = list.values.maxStamina;
        curStamina = maxStamina;
        experience = list.values.experience;

    }

}
