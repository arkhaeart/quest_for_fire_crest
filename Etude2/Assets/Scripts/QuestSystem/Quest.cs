﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Quests
{
    [CreateAssetMenu(menuName = "FireCrest/Quest")]
    public class Quest : ScriptableObject
    {
        public string ID;
        [TextArea]
        public string description;
        public Objective[] failures;
        public Stage[] stages;
        public string currentStage="none";
        public Stage CurrentStage {
            get
            {
                if (currentStage != "none")
                    return GetStage(currentStage);
                else
                    return null;
            }
            set
            {
                currentStage = value.ID;
            }
        }
        public List<Affiliate> affiliates=new List<Affiliate>();
        Stage GetStage(string _ID)
        {
            foreach(Stage s in stages)
            {
                if (s.ID == _ID)
                    return s;
            }
            throw new System.Exception("invalid stage ID");
        }

        public void Start()
        {
            currentStage = "0";

        }
        public void Fail()
        {
            currentStage = "fail";
        }
        public bool Update()
        {
            if (CurrentStage.success.result == QuestState.END)
            { currentStage = "end";
                return true;
            }
            currentStage = CurrentStage.success.IDtoGo;
            return false;
        }

        public void InitAffiliates()
        {
            currentStage = "none";
            affiliates.Clear();
            foreach(Objective fail in failures)
            {
                Affiliate aff = new Affiliate(fail);
                affiliates.Add(aff);
                fail.accomp = false;
            }
            foreach(Stage stage in stages)
            {
                foreach(Objective goal in stage.objectives)
                {
                    Affiliate aff = new Affiliate(goal);
                    affiliates.Add(aff);
                    goal.accomp = false;
                }
            }
        }
    }

    [System.Serializable]
    public struct Affiliate
    {
        public string ID;
        public ObjectType type;
        public Affiliate(Objective goal)
        {
            ID = goal.condition.name;
            type = goal.condition.objType;
        }
    }
    [System.Serializable]
    public class Stage
    {
        public string ID;
        [TextArea]
        public string description;
        public Objective[] objectives;
        public EVent success;

    }

    [System.Serializable]
    public class EVent
    {
        public string ID;
        public QuestState result;
        public string IDtoGo;
    }
    [System.Serializable]
    public class Objective
    {
        public string ID;
        public string description;
        public Condition condition;
        public bool accomp = false;
    }
    [System.Serializable]
    public class Condition
    {

        public string name;
        public InteractionType misType;
        public ObjectType objType;
        public int number = 0;
        public string subParam;
        public List<GameObject> objects = new List<GameObject>();
    }
    public class EnumeredCondition
    {
        public Condition parent;
        public int desired;
        public int current;
        public EnumeredCondition(Condition cond)
        {
            parent = cond;
            desired = cond.number;
            current = 0;
        }

    }
    public enum InteractionType
    {
        DESTROY,
        DEFEND,
        SEEK,
        BRING,
        INTERACT
    }
    public enum ObjectType
    {
        UNIT,
        ITEM,
        BASE,
        LOCATION,
        OBJECT
    }
}

