﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Quests;


public class QuestSystem : MonoBehaviour
{
    public GameObject questPanel;
    public QuestPanelManager panelManager;
    public Quest[] quests;
    List<Quest> activeQ = new List<Quest>();
    Quest unActiveQ;
    List<Log> log=new List<Log>();
    List<EnumeredCondition> enumered=new List<EnumeredCondition>();
    private void Start()
    {


        quests = Resources.LoadAll<Quest>("Quests");
        foreach(Quest q in quests)
        {
            q.currentStage = null;
            q.InitAffiliates();
        }
        StartCoroutine(TimedCheck());
    }
    private void Update()
    {
        if(Input.GetButtonDown("Quest"))
        {
            questPanel.SetActive(!questPanel.activeSelf);
        }
    }
    public void QuestAwake(string ID)
    {
        StartQuest(GetQuest(ID));
    }
    public void QuestReaction(string ID,int index)
    {
        Quest quest = GetQuest(ID);
        if(quest.CurrentStage.ID==index.ToString())
        {
            foreach (Objective goal in quest.CurrentStage.objectives)
            {
                if(goal.condition.misType==InteractionType.BRING)
                {
                    if (Inventory.GetItemCount(goal.condition.name) == goal.condition.number)
                    {
                        Inventory.RemoveItem(ItemDatabase.GetItem(goal.condition.name), GameManager.player.GetComponent<Char>().rpg, goal.condition.number);
                        goal.accomp = true;
                    }
                }
                else if(goal.condition.misType==InteractionType.INTERACT)
                {
                    if (goal.condition.name == GameManager.dialogManager.speaker.GetComponent<FCObject>().questReference)
                    {
                        goal.accomp = true;
                    }
                }

            }
        }
    }

    public void QuestObjectCheck(string ID,InteractionType type)
    {
        List<Quest> linkedQuests = new List<Quest>();
        for (int i = 0; i < quests.Length; i++)
        {
            for (int y = 0; y < quests[i].affiliates.Count; y++)
            {
                if(quests[i].affiliates[y].ID==ID)
                {
                    linkedQuests.Add(quests[i]);
                }
            }
        }
        if(linkedQuests.Count>0)
        {
            AddLogEntry(ID, type);
            foreach(Quest quest in linkedQuests)
            {
                if (quest.CurrentStage != null)
                {
                    foreach (Objective goal in quest.CurrentStage.objectives)
                    {
                        if (goal.condition.name == ID)
                        {
                            if (goal.condition.misType == type)
                            {
                                GoalMet(goal);
                            }
                        }
                    }
                }
                foreach (Objective fail in quest.failures)
                {
                    Debug.Log(fail.ID);
                    if(fail.condition.name==ID)
                    {
                        if(fail.condition.misType==type)
                        {
                            GoalMet(fail);
                        }
                    }
                }
            }
        }
    }
    public bool CheckPrereq(string[] req)
    {
        Quest quest = GetQuest(req[1]);
        Debug.Log(quest);
        if (quest.currentStage == req[2])
                return true;
        else
                return false;
    }
    IEnumerator TimedCheck()
    {
        while(true)
        {
            CheckActiveQuests();
            yield return new WaitForSeconds(1f);
        }
    }
    void CheckActiveQuests()
    {
        
        if (activeQ.Count == 0)
            return;
        List<Quest> qToRemove = new List<Quest>();
        foreach(Quest q in activeQ)
        {
            unActiveQ = null;
            Action action = CheckQuestState(q);
            if (action != null)
            {
                action.Invoke();
            }
            if(unActiveQ!=null)
            {
                qToRemove.Add(unActiveQ);
            }
        }
        foreach(Quest q in qToRemove)
        {
            activeQ.Remove(q);
        }
        

    }
    #region CheckSystems
    void GoalMet(Objective goal)
    {
        if(goal.condition.number==0)
        {
            goal.accomp = true;
        }
        else
        {
            EnumeredCondition cond =GetOrCreateEnumCond(goal.condition);
            cond.current++;
            CheckEnumCounter(cond);
            if(cond.current==cond.desired)
            {
                goal.accomp = true;
            }
        }
    }
    void CheckEnumCounter(EnumeredCondition cond)
    {
        Condition condition = cond.parent;
        if(condition.objType==ObjectType.ITEM&&condition.misType==InteractionType.SEEK)
        {
            cond.current=Inventory.GetItemCount(condition.name);
            Debug.Log(cond.current);
        }
        else
        {
            int total = 0;
            foreach(Log lg in log)
            {
                if (lg.ID == condition.name&&lg.type==condition.misType)
                {
                    total++;
                }
            }
            cond.current = total;
        }
    }
    Action CheckQuestState (Quest quest)
    {
        Action action = null;
        if(CheckFail(quest))
        {
            action = () => { FailQuest(quest); };
        }
        else if(CheckQuestGoals(quest.CurrentStage))
        {
            action = () => { UpdateQuest(quest); };
        }
        return action;
    }
    bool CheckQuestGoals(Stage stage)
    {
        int i = 0;
        foreach(Objective goal in stage.objectives)
        {
            if(goal.accomp)
                i++;
        }
        if (i == stage.objectives.Length)
            return true;
        else return false;
    }
    bool CheckFail(Quest quest)
    {
        foreach(Objective goal in quest.failures)
        {
            if (goal.accomp)
                return true;
        }
        return false;
    }
    EnumeredCondition GetOrCreateEnumCond(Condition cond)
    {
        foreach (EnumeredCondition condition in enumered)
        {
            if (condition.parent == cond)
                return condition;
        }
        EnumeredCondition newcond = new EnumeredCondition(cond);
        enumered.Add(newcond);
        return newcond;
    }
    void DeleteEnumFromList(EnumeredCondition cond)
    {
        enumered.Remove(cond);
    }
    #endregion

    void StartQuest(Quest quest)
    {
        activeQ.Add(quest);
        quest.Start();
        panelManager.ProcessQuest(quest,QuestState.START);
    }
    void UpdateQuest(Quest quest)
    {
        bool end = quest.Update();
        if (end)
        {
            panelManager.ProcessQuest(quest, QuestState.END);
            unActiveQ = quest;
        }
        else
            panelManager.ProcessQuest(quest, QuestState.UPDATE);
    }
    void FailQuest(Quest quest)
    {
        unActiveQ = quest;
        panelManager.ProcessQuest(quest, QuestState.FAIL);
    }

    public Quest GetQuest(string ID)
    {
        foreach (Quest quest in quests)
        {
            if (quest.ID == ID)
                return quest;
        }
        return null;
    }

    #region LogTools
    void AddLogEntry( string ID,InteractionType type)
    {
        Log entry = new Log(ID, type);
        log.Add(entry);
    }
    public class Log
    {
        public string ID;
        public InteractionType type;
        public Log(string _ID, InteractionType _type)
        {
            ID = _ID;
            type = _type;
        }
    }
    #endregion
}
public enum QuestState
{
    HIDDEN,
    START,
    UPDATE,
    END,
    FAIL
}
