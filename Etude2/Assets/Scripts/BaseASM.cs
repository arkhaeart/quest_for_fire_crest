﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimStates
{
    BATTLE,
    ATTACK,
    BEINGHIT,
    DEAD,
    CASTING

}

public class BaseASM : StateMachineBehaviour
{
    public Char character;
    public int MoveDir;
    public Animator animator;
    int[,] dir;
    public int lastDir = 1;
    public SubSM[] movSub;

    
    public void InitBattle()
    {
        MoveDir = lastDir;
        animator.SetTrigger("ToBattle");
    }
    public void MoveDirection(Vector3 direction)
    {
        dir = MovDirections.GetDirections;
        if (Mathf.Abs(direction.x) <= 0.15f && direction.y != 0||Mathf.Abs(direction.x)*5<=Mathf.Abs(direction.y))
            direction.x = 0;
        else if (Mathf.Abs(direction.y) <= 0.15f && direction.x != 0|| Mathf.Abs(direction.y) * 5 <= Mathf.Abs(direction.x))
            direction.y = 0;
        int x = direction.x.GetAxis();
        int y = direction.y.GetAxis();
        MoveDir = dir[x + 1, y + 1];
        animator.SetInteger("MoveDir", MoveDir);
        if (animator.GetBool("IsMoving") != true)
        {
            animator.SetTrigger("Move");
        }
        animator.SetBool("IsMoving", true);
    }
    void ChangeState()
    {
        animator.SetInteger("MoveDir", 0);
        animator.SetInteger("MoveDir", MoveDir);
    }
    public void SetDir()
    {
        
        //animator.SetInteger("MoveDir", MoveDir);
    }
    public void Attack()
    {
        
        animator.SetTrigger("Attack");
        
    }
    public void BeenHit()
    {
        animator.SetTrigger("BeingHit");
        
    }
    public void Dead()
    {
        animator.SetTrigger("Killed");
    }
    public void StopAnimation()
    {
        animator.SetTrigger("Stop");
    }
    public void StopMoving()
    {

        if (animator.GetBool("IsMoving") != false)
        {
            animator.SetTrigger("Stop");

        }
        animator.SetBool("IsMoving", false);
    }
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (SubSM SM in movSub)
        {
            SM.baseASM = this;
        }
        
   }

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMachineEnter is called when entering a state machine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}

    // OnStateMachineExit is called when exiting a state machine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}
}
