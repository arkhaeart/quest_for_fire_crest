﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHandlerScript : MonoBehaviour
{
    public static UnitHandlerScript instance;
    public bool ImplementOnRun;
    public UnitHandler unitHandler;
    public List<Char> unitList=new List<Char>();
    public List<Dialog> dialogList=new List<Dialog>();
    public List<RPGStatsMain> statsList=new List<RPGStatsMain>();
    public List<InventoryList> invList=new List<InventoryList>();
    public PatrolWay[] wayList;
    public PatrolSystem patrolSystem;
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        patrolSystem = GetComponent<PatrolSystem>();
        RegisterAll();
        patrolSystem.FillArrays(UnitNumber);
       
        
    }
    private void Start()
    {

        
    }
    public void Reset()
    {
        ClearLists();
    }
    public static int UnitNumber
    {
        get
        {
            return instance.unitList.Count;
        }
    }
    public void ClearLists()
    {
        unitList.Clear();
        dialogList.Clear();
        statsList.Clear();
        invList.Clear();
    }
    static MovingUnit[] ObjectsToMoving(List<Char> units)
    {
        MovingUnit[] retUnits = new MovingUnit[units.Count];
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i] is NPC)
                retUnits[i] = units[i].GetComponent<MovingUnit>();
            else
                retUnits[i] = null;
        }
        return retUnits;
    }
    public static MovingUnit[] GetPatrolArrays(out PatrolWay[] ways)
    {
        MovingUnit[] units = ObjectsToMoving(instance.unitList);
        ways = instance.unitHandler.wayList;
        for (int i = 0; i < ways.Length; i++)
        {
           
        }
        return units;
    }
    public void CheckRegistration()
    {

    }
    public void RegisterAll()
    {
        
        Char[] allObjects = FindObjectsOfType<Char>();
        for (int i = 0; i < allObjects.Length; i++)
        {
            if(!unitList.Contains(allObjects[i]))
                RegisterUnit(i,allObjects[i]);
        }
        
    }
    public void RegisterUnit(int index, Char gameObject)
    {
        Char unitChar = gameObject.GetComponent<Char>();
        unitChar.unitRegNumber = index;
        unitList[index]=gameObject;
        dialogList[index] = unitChar.testDialog;
        statsList[index] = unitChar.RPGStats;
        invList[index] = unitChar.inventoryList;
        wayList[index] = GetComponent<PatrolSystem>().patrolWays[index];
    }
    public void SaveChanges()
    {
        for (int i = 0; i < unitList.Count; i++)
        {
            
            unitHandler.dialogList[i] = dialogList[i];
            unitHandler.statsList[i] = statsList[i];
            unitHandler.invList[i] = invList[i];
            unitHandler.wayList[i] = new PatrolWay(wayList[i].wayTPoints);
        }
        
    }
    public void ImplementChanges()
    {
        GetComponent<PatrolSystem>().patrolWays = new PatrolWay[unitList.Count];
        for (int i = 0; i < unitList.Count; i++)
        {
            unitList[i].testDialog = unitHandler.dialogList[i];
            unitList[i].RPGStats = unitHandler.statsList[i];
            unitList[i].inventoryList = unitHandler.invList[i];
            patrolSystem.patrolWays[i] = new PatrolWay(wayList[i].wayPoints);
        }
    }
    

}
