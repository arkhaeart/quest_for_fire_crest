﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    public int currentLevel;
    public List<LevelExit> exits;
    
    private void Awake()
    {
        instance = this;
        if (instance != this)
            Destroy(gameObject);
    }
    public LevelExit GetExit(int number)
    {
        foreach(LevelExit ex in exits)
        {
            if (ex.entrance == number)
                return ex;   
        }
        throw new System.Exception("invalid entrance ID");
    }
}
