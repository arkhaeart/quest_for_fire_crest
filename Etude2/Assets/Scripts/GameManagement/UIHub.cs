﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public enum InvAction
{
    EXCHANGE,
    EQUIP,
    UNEQUIP,
    DROP,
    DEFAULT
}
public class UIHub : MonoBehaviour,IPointerDownHandler,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    static UIHub instance;
    public GameObject gameManager;
    public Text PopUp;
    public Text InvInfo;
    Text HUDLog;
    public static UnityEvent useEvent=new UnityEvent();
    public static PlInvManager plInvManager;
    public static ExchInvManager exchInvManager;
    public static GameObject equipmentWindow;
    public GameObject player;
    public GameObject DnDPlatform;
    public static InvSlotHolder holder;
    public static InvSlot currentSlot;
    public static InvSlot receiverSlot;
    public static bool dropoff;
    GameObject currentContainer;
    bool blockClick = false;
    GraphicRaycaster raycaster;
    EventSystem eventSystem;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        GameManager.hub = this;
        GameManager.dialogManager = GetComponentInChildren<DialogManager>(true);
        GameManager.panelManager = transform.Find("QuestPanel").GetComponent<QuestPanelManager>();
        GameManager.battleDialogManager = GetComponentInChildren<BattleDialogManager>(true);
        
        raycaster = GetComponent<GraphicRaycaster>();
        eventSystem = GetComponent<EventSystem>();
        HUDLog = transform.Find("HUD").GetChild(0).GetComponentInChildren<Text>();
    }
    void Start()
    {
        plInvManager = GameManager.plInvManager;
        exchInvManager = GameManager.exchInvManager;
        equipmentWindow =plInvManager.equipmentWindow;
    }
    static void DisplayText(string log)
    {
        instance.HUDLog.text = log;
    }
    void GetInfo(PointerEventData eventData)
    {
        InvSlot slot = GetFirstGrRaycast(eventData);
        if (slot == null || slot.belonging == "default"||slot.item==null)
            return;
        Item item = ItemDatabase.gameItemsDictionary[slot.item];
        item.GetItemDescription(out string[] info);
        WriteInfo(info);

    }
    void WriteInfo(params string[] info)
    {
        string all = string.Join("\n", info);
        InvInfo.text = all;
    }
    public static void ClickResponse(GameObject obj)
    {
        Debug.Log(obj.name);
        DisplayText("This is " + obj.name);
    }
#region Interaction
    public void InitInteraction(GameObject obj)
    {
        NoInteraction();
        useEvent.AddListener(()=>{ Interact(obj); });
        PopUp.text = string.Format("Press E to interact with {0}", obj.name);
        PopUp.gameObject.SetActive(true);
        StartCoroutine(WaitForResponse());
    }
    public void NoInteraction()
    {
        useEvent.RemoveAllListeners();
        PopUp.gameObject.SetActive(false);
        StopCoroutine(WaitForResponse());
    }
    public void CheckContainer(GameObject container)
    {
        currentContainer = container;
    }
    IEnumerator WaitForResponse()
    {
        while (!Input.GetButtonDown("Use"))
            yield return null;
        useEvent.Invoke();

    }
    public void Interact(GameObject obj)
    {
        useEvent.RemoveAllListeners();
        Debug.Log("Interacted with " + obj.name);

        if (obj.CompareTag("Item"))
        {
            if (plInvManager.IsFull)
            {
                Debug.Log("inventory is full");
                return;
            }
            ItemRef takenItem = obj.GetComponent<ItemRef>();
            Item itemRef = takenItem.itemRef;
            plInvManager.AddInv(itemRef);
            takenItem.QuestTrigger(Quests.InteractionType.SEEK);
            Destroy(obj);
        }
        else if (obj.CompareTag("Container"))
        {
            exchInvManager.InitContainer(obj);
            StartCoroutine(WaitForFinish(obj));
        }
        else if (obj.CompareTag("Unit"))
        {
            GameManager.dialogManager.InitDialog(obj);
            obj.GetComponent<NPC>().StopMovement();
        }
        else if (obj.CompareTag("Exit"))
        {
            GameManager.instance.LoadScene(obj.GetComponent<LevelExit>().levelToGo,obj.GetComponent<LevelExit>().entranceToGo);
        }
        
    }
    IEnumerator WaitForFinish(GameObject obj)
    {
        currentContainer = null;
        while (obj != currentContainer)
        {
            yield return null;
        }
        FinishInteraction(obj);
    }
    void FinishInteraction(GameObject obj)
    {
                if (obj.CompareTag("Container"))
        {
            exchInvManager.CloseContainer();
        }
    }
#endregion

#region DragNDrop
    void ClearAll()
    {
        currentSlot = null;
        receiverSlot = null;
        holder = null;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        GetInfo(eventData);
        if (Input.GetMouseButtonDown(1))
        {
            if (!blockClick)
            {
                ClearAll();
                currentSlot = GetFirstGrRaycast(eventData);
                ProceedInventory();
            }
        }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        ClearAll();
        if (!Input.GetMouseButton(0))
            return;
        currentSlot = GetFirstGrRaycast(eventData);
        receiverSlot = null;
        if (Input.GetMouseButtonDown(1))
        {
            return;
        }
        if (currentSlot == null)
            return;
        else if (currentSlot.image == null || currentSlot.belonging == "default")
            return;
        else if (currentSlot.image.sprite == null)
            return;
        Color color = currentSlot.image.color;
        color.a = 0.5f;
        currentSlot.image.color = color;
        DnDPlatform.transform.position = currentSlot.transform.position;
        Image DnDImage = DnDPlatform.GetComponent<Image>();
        DnDImage.sprite = currentSlot.image.sprite;
        DnDPlatform.SetActive(true);
        blockClick = true;
    }
    public void OnDrag(PointerEventData eventData)
    {
        DnDPlatform.transform.position = Input.mousePosition;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        if (currentSlot == null)
            return;
        if(currentSlot.image!=null)
        {
                Color color = currentSlot.image.color;
                color.a = 1f;
                currentSlot.image.color = color;
        }
        if (currentSlot.belonging == "default")
            return;
        receiverSlot = GetFirstGrRaycast(eventData);
        
        DnDPlatform.SetActive(false);
        DnDPlatform.transform.position = Vector2.zero;
        blockClick = false;
        ProceedInventory();
    }
    InvSlot GetFirstGrRaycast(PointerEventData eventData)
    {
        List<RaycastResult> result = new List<RaycastResult>();
        raycaster.Raycast(eventData, result);
        if (result.Count != 0)
        {
            InvSlot slot=result[0].gameObject.GetComponent<InvSlot>();
            return slot;
        }
        else
            return null;
    }
    #region Procedures
    static void Exchange()
    {
        int number = currentSlot.slotNumber;

        //if (holder.to.IsFull)//works wrong for consumables
        //{ }
        Item noun = holder.from.RemoveInvWithReturn(number);
        if (noun == null)
            return;
        else
        {
            if (receiverSlot == null || receiverSlot.belonging == "default")
             holder.to.AddInv(noun);
            else
                holder.to.AddInv(noun, receiverSlot.slotNumber);
        }
    }
    static void Equip()
    {
        Item itemToAdd = holder.from.RemoveInvWithReturn(currentSlot.slotNumber);
        if (itemToAdd.GetType() == typeof(Weapon))
        {
            Weapon moItem = (Weapon)(itemToAdd);
            plInvManager.Equip(moItem);
        }
        else if (itemToAdd.GetType() == typeof(Armor))
        {
            Armor moItem = (Armor)(itemToAdd);
            plInvManager.Equip(moItem);
        }  
    }
    static void Drop()
    {
        Item toDrop;
        if(currentSlot.belonging!="inner"&& currentSlot.belonging != "outer")
        {
            toDrop = plInvManager.Unequip(currentSlot.belonging);
            currentSlot.image.enabled = false;
            currentSlot.image.raycastTarget = false;
        }
        else
        {
            toDrop = holder.from.RemoveInvWithReturn(currentSlot.slotNumber);
        }
        Inventory.DropItem(toDrop, GameManager.player);
    }
    static void UnEquip()
    {
        Item itemToMove = plInvManager.Unequip(currentSlot.belonging);
        if (receiverSlot == null||receiverSlot.belonging=="default")
            holder.to.AddInv(itemToMove); 
        else
            holder.to.AddInv(itemToMove, receiverSlot.slotNumber);
        currentSlot.image.enabled = false;
        currentSlot.image.raycastTarget = false;
    }

    #endregion
    public static void ProceedInventory()
    {
        holder = new InvSlotHolder();
        if(currentSlot==null)
        {
            return;
        }
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log(currentSlot.belonging);
            if (currentSlot.belonging == "default")
                return;
            if (exchInvManager.wholeInv.activeSelf)
            {
                if (currentSlot.belonging == "inner")
                    holder.to = exchInvManager;
                else
                    holder.to = plInvManager;

                Exchange();
            }
            else if (equipmentWindow.activeSelf)
            {
                if (currentSlot.belonging == "inner")
                {
                    Equip();
                }
                else
                {
                    holder.to = plInvManager;
                    UnEquip();
                }
            }
            else
            { }
        }
        else
        { 
            switch(holder.action)
            {
                case (InvAction.DROP):
                    Drop();

                    break;
                case (InvAction.EQUIP):
                    Equip();
                    break;
                case (InvAction.EXCHANGE):
                    Exchange();
                    break;
                case (InvAction.UNEQUIP):
                    UnEquip();
                    break;
                case (InvAction.DEFAULT):
                    break;
            }
        }
    }
}

[System.Serializable]
public class InvSlotHolder
{
    public InvSlot fromSlot;
    public InvSlot toSlot;
    public InvManager from;
    public InvManager to;
    public InvAction action;
    bool Check(InvSlot slot)
    {
        if (slot.belonging == "default" && slot.slotNumber != 2)
            return true;
        else
            return false;
    }
    public InvSlotHolder()
    {
        fromSlot = UIHub.currentSlot;
        toSlot = UIHub.receiverSlot;
        if (fromSlot.belonging == "inner")
            from = UIHub.plInvManager;
        else
            from = UIHub.exchInvManager;
        if (toSlot == null)
            action = InvAction.DROP;
        else if (toSlot != null)
        {
            if (toSlot.belonging == "inner")
                to = UIHub.plInvManager;
            else
                to = UIHub.exchInvManager;
            if (fromSlot.belonging == "inner" || fromSlot.belonging == "outer")
            {
                if (toSlot.belonging == "inner" || toSlot.belonging == "outer"||Check(toSlot))
                    action = InvAction.EXCHANGE;
                else
                    action = InvAction.EQUIP;
            }
            else
            {
                if (toSlot.belonging!="inner"&&toSlot.belonging!="outer"&&!Check(toSlot))
                    action = InvAction.DEFAULT;
                else
                    action = InvAction.UNEQUIP;
            }
            if (toSlot.belonging == "default")
            {
                UIHub.receiverSlot = null;
                if (toSlot.slotNumber == 0)
                    to = UIHub.plInvManager;
                else
                    to = UIHub.exchInvManager;
            }
        }
    }
    #endregion

}
