﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[System.Serializable]
public class PatrolSystem : MonoBehaviour
{
    static PatrolSystem instance;
    Queue<PatrolRequest> requests = new Queue<PatrolRequest>();
    bool isProcessing=false;
    

    public MovingUnit[] units;
    public PatrolWay[] patrolWays;

    List<MovingUnit> movingUnits=new List<MovingUnit>();
    PatrolRequest currentRequest;

    private void Awake()
    {
       
    }
    public void FillArrays(int unitNumber)
    {
        if (instance == null)
            instance = this;
        units = new MovingUnit[unitNumber];
        patrolWays = new PatrolWay[unitNumber];
        units=UnitHandlerScript.GetPatrolArrays(out patrolWays);
        for (int i = 0; i < unitNumber; i++)
                {
                    movingUnits.Add(units[i]);
                }
    }
    public static void GetNextWaypoint(MovingUnit unit, Action<Vector3, bool> callback)
    {
        
        PatrolRequest newRequest = new PatrolRequest(callback, unit);
        instance.requests.Enqueue(newRequest);
        instance.StartCoroutine(instance.ProcessRequest());
        



    }
    IEnumerator ProcessRequest()
    {
        if (!isProcessing && requests.Count > 0)
        {
            currentRequest = requests.Dequeue();
            isProcessing = true;

            if (true||movingUnits.Contains(currentRequest.unit))
            {
                
                int unitNumber = Array.IndexOf(units, currentRequest.unit);
                PatrolWay way = patrolWays[unitNumber];
                try
                {
                    currentRequest.callback(way.wayPoints[way.wayPointIndex], true);
                }
                catch(IndexOutOfRangeException)
                {
                    yield break;
                }
                PatrolWay.IndexPlus(patrolWays[unitNumber]);
                isProcessing = false;
            }
        }
        else
        { yield break; }
        yield return null;
    }
    
}

[System.Serializable]
public class PatrolWay
{
    public int wayPointIndex = 0;
    public Transform[] wayTPoints;
    public Vector3[] wayPoints;
    public static void IndexPlus(PatrolWay way)
    {
        way.wayPointIndex++;
        if (way.wayPointIndex +1> way.wayPoints.Length)
            way.wayPointIndex = 0;
    }
    public PatrolWay(Vector3[] _wayPoints)
    {
        wayPoints = new Vector3[_wayPoints.Length];
        wayPoints = _wayPoints;

    }
    public PatrolWay(Transform[] _wayPoints)
    {
        wayTPoints = new Transform[_wayPoints.Length];
        wayTPoints = _wayPoints;
        wayPoints = new Vector3[_wayPoints.Length];
        for (int i = 0; i < _wayPoints.Length; i++)
        {
            wayPoints[i] = _wayPoints[i].position;
        }
        
    }

}
struct PatrolRequest
{
    public MovingUnit unit;
    public Action<Vector3, bool> callback;
    public PatrolRequest(Action<Vector3,bool> _callback, MovingUnit _unit)
    {
        callback = _callback;
        unit = _unit;
    }
}
