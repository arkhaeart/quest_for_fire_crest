﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="FireCrest/UnitHandler")]
[System.Serializable]
public class UnitHandler : ScriptableObject
{
    public int[] unitRegNumbers;
    public string[] unitNames;
    public Dialog[] dialogList;
    public RPGStatsMain[] statsList;
    public InventoryList[] invList;
    public PatrolWay[] wayList;
    
}
