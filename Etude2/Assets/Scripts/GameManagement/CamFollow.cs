﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public GameObject target;
    Transform camPos;
    float smooth = 5f;
    Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        camPos = GetComponent<Transform>();
        offset=target.transform.position - camPos.position;
    
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 newCamPos = target.transform.position - offset;
        camPos.position = Vector3.Lerp(camPos.position, newCamPos, smooth * Time.deltaTime);
    }
}
