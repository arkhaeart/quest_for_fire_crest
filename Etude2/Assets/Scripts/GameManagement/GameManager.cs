﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(QuestSystem))]
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static BattleManager battleManager;
    public static DialogManager dialogManager;
    public static BattleDialogManager battleDialogManager;
    public static PlInvManager plInvManager;
    public static ExchInvManager exchInvManager;
    public static ItemDatabase itemDatabase;
    public static UnitHandlerScript unitHandler;
    public static QuestSystem quest;
    public static UIHub hub;
    public static QuestPanelManager panelManager;
    public static GameObject unitMap;
    public static GameObject player;
    GameObject _player;
    int _entranceToGo;
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        GetComps();

        
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    private void Start()
    {
        WriteReferences();
        InitAll();
    }
    public void OnSceneLoaded(Scene scene,LoadSceneMode loadSceneMode)
    {
        //LevelExit exit =LevelManager.instance.GetExit(_entranceToGo);
        //player.GetComponent<Rigidbody2D>().MovePosition(exit.transform.position);
        //player.transform.SetParent(unitMap.transform);
        Camera.main.GetComponent<Cinemachine.CinemachineBrain>().m_WorldUpOverride = player.transform;

    }
    void GetComps()
    {
        battleManager = GetComponent<BattleManager>();
        plInvManager = GetComponent<PlInvManager>();
        exchInvManager = GetComponent<ExchInvManager>();
        itemDatabase = GetComponent<ItemDatabase>();
        unitHandler = GetComponent<UnitHandlerScript>();
        unitMap = GetComponent<GridManager>().unitMap;
        quest = GetComponent<QuestSystem>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void WriteReferences()
    {
        dialogManager.battleManager =battleManager;
        dialogManager.questSystem = quest;
        dialogManager.player = player;
        quest.panelManager = panelManager;
        quest.questPanel = panelManager.gameObject;
        battleDialogManager.battleManager = battleManager;
        battleDialogManager.player = player;
    }
    void InitAll()
    {
        panelManager.Init();
    }
    public void LoadScene(int number,int entrance)
    {
        Debug.Log("Scene " + number + " is loading");
        _player = player;
        DontDestroyOnLoad(player);
        SceneManager.LoadScene(number, LoadSceneMode.Single);


    }
    void SavePlayer()
    {

    }
    
}
