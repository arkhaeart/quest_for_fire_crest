﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Random;

public static class ExtensionMethods 
{
    public static int GetAxis(this float number)
    {
        int axis = number > 0 ? 1 : number== 0 ? 0 : -1;
        return axis;
    }
    public static Vector2 RandPos(this Vector3 pos)
    {
        Vector2 newPos;
        newPos.x = Range(pos.x - 0.5f, pos.x + 0.5f);
        newPos.y = Range(pos.y - 0.5f, pos.y + 0.5f);
        return newPos;
    }
    public static void UIHeightMod(this RectTransform rt,int mod)
    {
        float x = rt.sizeDelta.x;
        float y = rt.sizeDelta.y;
        rt.sizeDelta = new Vector2(x, y + mod);
    }
}
