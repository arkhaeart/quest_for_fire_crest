﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
    public enum DrawPlayer
    {
        CURRENT,
        TARGET
    }
public class BattleInfo : MonoBehaviour
{

    public Text[] playerName, playerhp;
    public Slider[] playerSlider;
    public Image[] playerImage;
    public Image[] playersEq;
    BatlleCard[] combatants;
    public void InitInfo(Char[] player)
    {
        combatants = new BatlleCard[player.Length];
        for (int i = 0; i < combatants.Length; i++)
        {
            combatants[i] = new BatlleCard(player[i]);
        }
        DrawPlayer(combatants[0],global::DrawPlayer.CURRENT);
        DrawPlayer(combatants[1],global::DrawPlayer.TARGET);
    }
    void DrawPlayer(BatlleCard player,DrawPlayer draw)
    {
        int drawNumber = draw == global::DrawPlayer.CURRENT ? 0 : 1;
        int eqmod = drawNumber*4;
        playerName[drawNumber].text = player.Name;
        playerhp[drawNumber].text = player.hp;
        playerImage[drawNumber].sprite = player.image;
        playerSlider[drawNumber].maxValue = player.maxhealth;
        playerSlider[drawNumber].value = player.curhealth;
        for (int i = 0; i < player.Eq.Length; i++)
        {
            if (player.Eq[i] != null)
            {
                playersEq[i+eqmod].sprite = player.Eq[i];
                playersEq[i+eqmod].enabled = true;
            }
        }


    }

    class BatlleCard
    {
        public string Name, hp;
        public int curhealth,maxhealth;
        public Sprite image;
        public Sprite[] Eq=new Sprite[4];
        public BatlleCard(Char player)
        {
            Name = player.name;
            maxhealth = player.rpg.values.maxHealth;
            curhealth = player.rpg.values.curHealth;
            hp = curhealth + "/" + maxhealth;
            image= player.GetComponent<SpriteRenderer>().sprite;
            Item[] plEq = Equipment.GetAll(player.rpg);

            for (int i = 0; i < Eq.Length; i++)
            {
                if (plEq[i] != null)
                {
                    Eq[i] = plEq[i].appearance;
                }
            }
        }
    }
}
