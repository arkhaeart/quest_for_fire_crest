﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "FireCrest/BattleDialog")]
public class BattleDialog : Dialog
{
    public Entry preset;
    public BattleDialogActions[] battleDialogActions=new BattleDialogActions[8];
}
