﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using Random = UnityEngine.Random;
public class BattleManager : MonoBehaviour
{
    public static int currentCombatant=0;
    public static Char[] combatants;
    public static int currentTurnNumber=0;
    public static bool turnSwitcher=false;
    public static bool battleIsOver = false;
    public static bool finishedHit = false;
    public enum Turn { Player,Enemy};
    public Turn currentTurn;
    public BattleDialog battleDialog;
    public BattleDialogManager dialogManager;
    public BattleInfo battleInfo;
    GameObject blockingPanel;
    public float refreshRate = 0.1f;
    public delegate IEnumerator CourToCall();
    public List<string> battleLog = new List<string>();
    public Text displayText;

    Char curDefence;
    
    CourToCall courToCall;
    

    void Start()
    {
        battleLog.Clear();
        DisplayText();
        blockingPanel = dialogManager.transform.Find("BlockingPanel").gameObject;
        battleInfo = dialogManager.GetComponentInChildren<BattleInfo>();
    }

    public void DisplayText()
    {
        string logAsText = string.Join("\n", battleLog.ToArray());
        displayText.text = logAsText;
    }
    public void InitBattle(Char[] combs)
    {
        blockingPanel.SetActive(true);
        battleIsOver = false;
        battleLog.Clear();
        dialogManager.gameObject.SetActive(true);
        currentTurnNumber = 0;
        combatants = new Char[combs.Length];
        combatants = combs;
        for (int i = 0; i < combatants.Length; i++)
        {
            combatants[i].ProcessBattleAnimations(AnimStates.BATTLE);
        }
        battleInfo.InitInfo(combatants);
        WhoseTurn();
        StartCoroutine(courToCall());
        BattleLog("Battle starts!");
    }
    public void EndBattle()
    {
        combatants = null;
        dialogManager.gameObject.SetActive(false);
    }
    public void WhoseTurn()
    {

        if (battleIsOver)
            return;
        if(combatants[currentCombatant].CompareTag("Player"))
        {
            currentTurn = Turn.Player;
            courToCall += PlayerTurn;
        }
        else
        {
            currentTurn = Turn.Enemy;
            courToCall += EnemyTurn;
        }
    }

    public void OnTurnEnd()
    {
        if (battleIsOver)
            return;
        currentCombatant++;
        
        if (combatants.Length < currentCombatant+1)
        {
            currentCombatant = 0;
            currentTurnNumber++;
        }
        battleInfo.InitInfo(combatants);
        WhoseTurn();
        StartCoroutine(courToCall());


    }
 
    public IEnumerator PlayerTurn()
    {
        dialogManager.InitDialog(battleDialog);
        blockingPanel.SetActive(false);
        while (!turnSwitcher)
        {
            yield return null;
        }
        blockingPanel.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        turnSwitcher = false;
        courToCall -= PlayerTurn;
        OnTurnEnd();
    }

    public IEnumerator EnemyTurn()
    {
        
        EnemyPerform();
        while (!turnSwitcher)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1.5f);
        turnSwitcher = false;
        courToCall -= EnemyTurn;
        OnTurnEnd();
    }
    public void EnemyPerform()
    {
        NPC npc = (NPC)combatants[currentCombatant];
        Weapon npcweapon = npc.rpg.weapon;
        string npcItemAndAttackPos =npcweapon.posAttacks[Random.Range(0, npcweapon.posAttacks.Length)].weaponPart;
        BattleAction(npc.battleAI.defaultAction, npcItemAndAttackPos);
    } 
    public void BattleAction(BattleActions action, string answerID)
    {
        switch (action)
        {

            case (BattleActions.ATTACK):


                int attackedNumber = currentCombatant == 0 ? 1 : 0;
                AttackStart(combatants[currentCombatant], combatants[attackedNumber], answerID);
                if (battleIsOver)
                {
                    turnSwitcher = true;
                    return;
                }
                dialogManager.InitDialog(battleDialog);
                turnSwitcher = true;
                break;
            case (BattleActions.USE):
                string[]consLog= ItemUsage.UseItem(answerID, combatants[currentCombatant].GetComponent<Char>());
                string newconsLog = string.Join(" ", consLog);
                BattleLog(newconsLog);
                turnSwitcher = true;
                return;
            case (BattleActions.WAIT):
                turnSwitcher = true;
                return;
        }
    }
    /*async void Attack(Char offence, Char defence, string itemAndAttackPos)
    {
        finishedHit = false;
        curDefence = defence;
        bool isKilled = false;
        bool hasHit = true;
        RPG atkStats = offence.rpg;
        RPG defStats = defence.rpg;
        Weapon weapon = offence.rpg.weapon;
        RawDamage rawDamage;
        int hpLoss = 0;
        for (int i = 0; i < weapon.posAttacks.Length; i++)
        {
            if (weapon.posAttacks[i].weaponPart == itemAndAttackPos)
            {
                rawDamage = weapon.posAttacks[i];
                hpLoss = DamageCalculations(rawDamage, offence, defence, out hasHit, out isKilled);
                break;
            }
        }
        offence.ProcessBattleAnimations(AnimStates.ATTACK);
        Task waitForRes =await WaitFor();
        waitForRes.Start();
        waitForRes.Wait();
    }
    Task WaitFor()
    {
        System.Action<object> action = (object obj) => { WaitForHit(); };
        Task task = new Task(action, "fuck");
        return task;
    }*/
    void AttackStart(Char offence, Char defence, string itemAndAttackPos)
    {
        finishedHit = false;
        curDefence = defence;
        bool isKilled = false;
        bool hasHit = true;
        RPG atkStats = offence.rpg;
        RPG defStats = defence.rpg;
        Weapon weapon = offence.rpg.weapon;
        RawDamage rawDamage;
        
        int hpLoss = 0;
        for (int i = 0; i < weapon.posAttacks.Length; i++)
        {
            if (weapon.posAttacks[i].weaponPart == itemAndAttackPos)
            {
                rawDamage = weapon.posAttacks[i];
                hpLoss = DamageCalculations(rawDamage, offence,defence,out hasHit,out isKilled);
                break;
            }
        }
        offence.ProcessBattleAnimations(AnimStates.ATTACK);

        StartCoroutine("WaitForHit");

        
        if (hasHit)
        {
            BattleLog(offence.name + " attacked " + defence.name + " with " + "\n" + weapon.noun + " " + itemAndAttackPos);
            BattleLog("Dealt " + hpLoss + " points of damage.");
            if (isKilled)
            {
                BattleLog(defence.name + " is dead!");
                defence.Death();
                for (int i = 0; i < combatants.Length; i++)
                {
                    if (combatants[i] == defence)
                    {
                        combatants[i] = null;
                    }
                }
                battleIsOver = true;
                BattleLog("Battle is over!");
                BattleLog(offence.name + " has won!");
                Invoke("EndBattle", 3f);
            }
        }
        else
        {
            BattleLog(offence.name + " missed " + defence.name + " !");
        }
    }
    void AttackMiddle(Char defence)
    {
        if (defence.rpg.values.curHealth <= 0)
            defence.ProcessBattleAnimations(AnimStates.DEAD);
        else
            defence.ProcessBattleAnimations(AnimStates.BEINGHIT);
        
    }
    IEnumerator WaitForHit()
    {
        while(!finishedHit)
        {
            yield return null;
        }
        AttackMiddle(curDefence);
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public int DamageCalculations(RawDamage rawDamage, Char off, Char def, out bool hit, out bool kill)
    {
        RPG atkStats = off.rpg;
        RPG defStats = def.rpg;
        int hpLoss = rawDamage.rawDamage;
        ArmorProps armor = Equipment.GetArmorProps(defStats);
        Debug.Log(armor.armorClass +" "+ armor.physicalDefense);
        kill = false;
        hit = true; //hit against block and evade evaluation
        if (!hit)
        { 
            return 0;
        }
        int puredamage = hpLoss;
        int calcdamage = Mathf.Clamp(puredamage / 3 - armor.armorClass, 0, 1000) +
            Mathf.Clamp(puredamage / 3 * (100 - armor.physicalDefense) / 100, 0, 1000) +
            Mathf.Clamp(puredamage / 3 * (100 - Mathf.RoundToInt(defStats.stats.DamageReduce)) / 100, 0, 1000);
        Debug.Log(calcdamage);
        /*
        
         rawdamage*attacker skills and stats
         calcdamage=Clamp(puredamage/3 - armor.armorClass) + clamp(puredamage/3*armor.physicalDefence/100)+clamp(puredamage/3*endurmod/100)
         
         
         
         */
        defStats.values.curHealth -= calcdamage;
        if (defStats.values.curHealth <= 0)
            kill = true;
        return calcdamage;
    }
    public void BattleLog(string text)
    {
        battleLog.Add(text+"\n");
        DisplayText();
    }
}
