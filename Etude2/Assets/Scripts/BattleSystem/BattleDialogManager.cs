﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
    public enum BattleActions
    {
        ATTACK,
        CAST,
        ABILITY,
        USE,
        WAIT,
        DEFAULT
        //MOVE

    }
public class BattleDialogManager : DialogManager
{

    [HideInInspector] public BattleActions currentAction;
    BattleDialog curDialog;

    protected override void Awake()
    {
        base.Awake();
        
    }
    public void InitDialog(BattleDialog battleDialog)
    {
        ClearDialogWindow();
        curDialog = battleDialog;
        ClearDialog();
        for (int i = 0; i < curDialog.entries.Length; i++)//this must be implemented to DialogManager as well
        {
            if  (curDialog.entries[i].entryID=="default")
            {
                currentEntry = curDialog.entries[i];
                break;
            }
        }
        
        PackEntries();
        UnpackEntry();
    }
    public void PackEntries()
    {
        RPG rpg = player.GetComponent<Char>().rpg;

        Item[] playerInventory = rpg.inventory;
        for (int i = 0; i < curDialog.entries.Length; i++)
        {
            switch(curDialog.entries[i].entryID)
            {
                    case ("attack"):
                    if (rpg.weapon != null)
                    {
                        curDialog.entries[i].dialogAnswers = new DialogAnswers[rpg.weapon.posAttacks.Length+1];
                        for (int j = 0; j < rpg.weapon.posAttacks.Length; j++)
                        {
                                curDialog.entries[i].dialogAnswers[j] = new DialogAnswers();
                                curDialog.entries[i].dialogAnswers[j].answerID = rpg.weapon.posAttacks[j].weaponPart;
                                curDialog.entries[i].dialogAnswers[j].answerText = "Hit with " + rpg.weapon.posAttacks[j].weaponPart;
                                curDialog.entries[i].dialogAnswers[j].actionResponse = "attack";
                        }
                        curDialog.entries[i].dialogAnswers[curDialog.entries[i].dialogAnswers.Length - 1] = curDialog.preset.dialogAnswers[0];
                    }
                    else
                        {
                        //Unarmedscripts
                        }
                        break;
                    case ("use"):
                    Consumable[] consumesInInventory;
                    int consumesLength=0;
                    int[] consumesNumber = new int[8];
                    for (int j = 0; j < rpg.inventory.Length; j++)
                    {
                        if (rpg.inventory[j] == null)
                            continue;
                        if(rpg.inventory[j] is Consumable)
                        {
                            consumesNumber[consumesLength] = j;
                            consumesLength++;
                            
                        }
                    }
                    consumesInInventory = new Consumable[consumesLength];
                    curDialog.entries[i].dialogAnswers = new DialogAnswers[consumesLength+1];
                    for (int j = 0; j < consumesLength; j++)
                    {
                        curDialog.entries[i].dialogAnswers[j] = new DialogAnswers();
                        consumesInInventory[j] = (Consumable)rpg.inventory[consumesNumber[j]];
                        curDialog.entries[i].dialogAnswers[j].answerID = rpg.inventory[consumesNumber[j]].noun;
                        curDialog.entries[i].dialogAnswers[j].answerText = "Use " + rpg.inventory[consumesNumber[j]].noun;
                        curDialog.entries[i].dialogAnswers[j].actionResponse = "use";
                    }
                    curDialog.entries[i].dialogAnswers[curDialog.entries[i].dialogAnswers.Length - 1] = curDialog.preset.dialogAnswers[0];
                    break;
                    
            }
        }
        

    }
    private void OnDisable()
    {
        ClearDialog();
    }
    void ClearDialog()
    {
        foreach(Entry en in curDialog.entries)
        {
            if(en.entryID!="default")
                en.dialogAnswers = null;
        }
    }
    public override void OnGetButtonResponse(string answerID)
    {
        for (int i = 0; i < currentEntry.dialogAnswers.Length; i++)
        {
            if (currentEntry.dialogAnswers[i].answerID == answerID)
            {
                currentAnswer = currentEntry.dialogAnswers[i];
                break;
            }

        }
        Debug.Log(currentAnswer.answerText);
        if (currentAnswer.actionResponse != "")
        {
            Debug.Log(currentAnswer.actionResponse);
            BattleDialog battleDialog = (BattleDialog)curDialog;
            for (int i = 0; i < battleDialog.battleDialogActions.Length; i++)
            {
                if (currentAnswer.actionResponse == battleDialog.battleDialogActions[i].ActionID)
                {
                    currentAction = battleDialog.battleDialogActions[i].battleActions;
                    break;
                }
            }
            if (currentAction != BattleActions.DEFAULT)
            {
                battleManager.BattleAction(currentAction, currentAnswer.answerID);
                return;
            }
        }
        int currentExitNumber = 0; //now primitivized, to be changed

        for (int i = 0; i < curDialog.entries.Length; i++)
        {
            if (currentAnswer.dialogExits[currentExitNumber].entryToGoTo == curDialog.entries[i].entryID)
            {

                currentEntry = curDialog.entries[i];
                UnpackEntry();
                return;
            }
        }
        //base.OnGetButtonResponse(answerID);
    }

}
