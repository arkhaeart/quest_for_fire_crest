﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RawDamage
{
    public string weaponPart;
    public DamageType damageType;
    public int rawDamage;

}
