﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Quests;

public class QuestPanelManager : MonoBehaviour
{
    public GameObject qHolderPrefab;
    public GameObject qGoalPrefab;
    public Sprite qGreen, qRed;
    Quest currentDrawn;
    public Transform contents;
    Transform currentPanel, endPanel, failPanel,goalPanel;
    Text descrPanel;
    List<QuestHolder> drawnHolders=new List<QuestHolder>();
    List<GameObject> drawnGoals = new List<GameObject>();
    public void Init()
    {

        currentPanel = contents.Find("CurrentPanel");
        endPanel = contents.Find("EndPanel");
        failPanel = contents.Find("FailPanel");
        goalPanel = transform.Find("GoalPanel");
        descrPanel = transform.Find("DescrPanel").gameObject.GetComponentInChildren<Text>(true);
    }
    public void ProcessQuest(Quest quest,QuestState state)
    {
        switch((int)state)
        {
            case 0:
                break;
            case 1:
                AddQuest(quest);
                break;
            case 2:
                DrawQuestInfo();
                break;
            case 3:
                MoveQuest(quest, endPanel);
                break;
            case 4:
                MoveQuest(quest, failPanel);
                break;
        }
    }
    void AddQuest(Quest quest)
    {
        currentPanel.GetComponent<RectTransform>().UIHeightMod(25);
        GameObject instance = Instantiate(qHolderPrefab, currentPanel);
        FillQuest(instance, quest);
    }
    void MoveQuest(Quest quest,Transform panel)
    {
        currentPanel.GetComponent<RectTransform>().UIHeightMod(-25);
        panel.GetComponent<RectTransform>().UIHeightMod(25);
        QuestHolder holder = GetHolder(quest.ID);
        holder.transform.SetParent(panel);
    }
    void FillQuest(GameObject instance, Quest quest)
    {
        instance.GetComponent<QuestHolder>().questID = quest.ID;
        drawnHolders.Add(instance.GetComponent<QuestHolder>());
        instance.GetComponentInChildren<Text>(true).text = quest.description;
    }
    QuestHolder GetHolder(string ID)
    {
        foreach(QuestHolder holder in drawnHolders)
        {
            if (holder.questID == ID)
                return holder;
        }
        return null;
    }
    public void GetDrawResponse(string ID)
    {
        Quest quest = GameManager.quest.GetQuest(ID);
        currentDrawn = quest;
        DrawQuestInfo();
    }
    void DrawQuestInfo()
    {
        if (currentDrawn == null)
            return;
        ClearQuestInfo();
        descrPanel.text = currentDrawn.CurrentStage.description;
        Stage stage = currentDrawn.CurrentStage;
        foreach(Objective goal in stage.objectives)
        {
            GameObject obj = Instantiate(qGoalPrefab, goalPanel);
            obj.GetComponentInChildren<Image>().sprite = qGreen;
            obj.GetComponentInChildren<Text>().text = goal.description;
            drawnGoals.Add(obj);
        }
        foreach(Objective goal in currentDrawn.failures)
        {
            GameObject obj = Instantiate(qGoalPrefab, goalPanel);
            obj.GetComponentInChildren<Image>().sprite = qRed;
            obj.GetComponentInChildren<Text>().text = goal.description;
            drawnGoals.Add(obj);
        }
    }
    void ClearQuestInfo()
    {
        descrPanel.text = "Выберите квест";   
        foreach(GameObject obj in drawnGoals)
        {
            Destroy(obj);
        }
        drawnGoals.Clear();
    }
}
