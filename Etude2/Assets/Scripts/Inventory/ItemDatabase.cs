﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    
    public EquipmentSlots equipmentSlots;
    public ArmorGrades armorGrades;
    public Item[] gameItems;//legacy
    public Database database;
    public static Dictionary<string, Item> gameItemsDictionary = new Dictionary<string, Item>();
    public static Dictionary<string, Armor> armorDict = new Dictionary<string, Armor>();
    public static List<string> slotDict = new List<string>();
    public static List<Weapon> weaponList = new List<Weapon>();
    public static Item GetItem(string ID)
    {
        if (gameItemsDictionary.ContainsKey(ID))
        {
            return gameItemsDictionary[ID];
        }
        else throw new System.Exception("Wrong item ID!");
    }
    private void Awake()
    {
        InitArmorDict();
        CreateDatabase();
    }
    Item[] LoadItems()
    {
        Item[] objects = Resources.LoadAll<Item>("GameItems");
        return objects;
    }
    public void CreateDatabase()
    {
       InitGameItemsDict();
       database = new Database(weaponList);
    }
    void InitGameItemsDict()
    {
        Item[] items = LoadItems();
        weaponList.Clear();
        gameItemsDictionary.Clear();
        for (int i = 0; i < items.Length; i++)
        {
            gameItemsDictionary.Add(items[i].noun, items[i]);
            if(items[i]is Weapon)
            {
                weaponList.Add((Weapon)items[i]);
            }
        }
    }
    void InitArmorDict()
    {
        for (int i = 0; i < equipmentSlots.eqSlots.Length; i++)
        {
            armorDict.Add(equipmentSlots.eqSlots[i], null);
            slotDict.Add(equipmentSlots.eqSlots[i]);
        }
    }
    public static Dictionary<string,Armor> InitArmor ()
    {
        Dictionary<string, Armor> dict = new Dictionary<string, Armor>();
        for (int i = 0; i < slotDict.Count; i++)
        {
            dict.Add(slotDict[i], null);
        }
        return dict;
        }
    private void Reset()
    {
        CreateDatabase();
    }
}
[System.Serializable]
public class Database
{

    public Item[] items;
    public string[] noun;
    public int[] value1;
    public int[] value2;
    public int[] value3;
    public int ItemCount
    {
        get
        {
            if (items == null)
                items = new Item[1];
            return items.Length;

        }
    }
    public Database(Item[] item)
    {
        items = item;
        noun = new string[item.Length];
        value1 = new int[item.Length];
        value2 = new int[item.Length];
        value3 = new int[item.Length];
        for (int i = 0; i < item.Length; i++)
        {
            noun[i] = item[i].noun;
            value1[i] = item[i].basicValue;
            value2[i] = 0;
            value3[i] = 0;
        }
    }
    public Database(List<Weapon> list)
    {

        items=new Item[list.Count];
        noun=new string[list.Count];
        value1=new int[list.Count];
        value2=new int[list.Count];
        value3=new int[list.Count];
        for (int i = 0; i < list.Count; i++)
        {
            items[i] = list[i];
            noun[i] = list[i].noun;
            for (int y = 0; y < list[i].posAttacks.Length; y++)
            {
                 
                switch(list[i].posAttacks[y].damageType)
                {
                    case (DamageType.CUT):
                        value1[i] = list[i].posAttacks[y].rawDamage;
                        break;
                    case (DamageType.PIERCING):
                        value2[i] = list[i].posAttacks[y].rawDamage;
                        break;
                    case (DamageType.BLUNT):
                        value3[i] = list[i].posAttacks[y].rawDamage;
                        break;


                }
               
            }
        }
    }
}

