﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExchInvManager : InvManager
{
    PlInvManager plInvManager;
    
    private void Awake()
    {
        plInvManager = GetComponent<PlInvManager>();
    }
    public void InitContainer(GameObject container)
    {
        player = container.GetComponent<Char>();
        rpg = player.rpg;
        for (int i = 0; i < rpg.inventory.Length ; i++)
        {
            if (rpg.inventory[i] != null)
                SlotProcess(i,rpg.inventory[i]);
            else continue;
        }
        wholeInv.SetActive(true);
    }
    public void CloseContainer()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].image.sprite = null;
        }
        wholeInv.SetActive(false);
    }
    public void ExchangingInventory()
    {

    }
}
