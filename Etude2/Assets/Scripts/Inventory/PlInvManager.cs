﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlInvManager : InvManager
{

    public InvSlot[] eqSlots;
    public GameObject equipmentWindow;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Char>();
        rpg = player.rpg;

        for (int i = 0; i < eqSlots.Length; i++)
        {
            eqSlots[i].image = eqSlots[i].GetComponent<Image>();
            eqSlots[i].slotNumber = i;
        }                 
    }
    public void Equip (Weapon itemToAdd)
    {
        Image image = eqSlots[0].image;
        Weapon itemToMove = Equipment.AddItem(rpg,itemToAdd);
        image.enabled = true;
        image.sprite = itemToAdd.appearance;
        image.raycastTarget = true;
        eqSlots[0].item = itemToAdd.noun;
        if (itemToMove != null)
            AddInv(itemToMove);
    }
    public void Equip(Armor itemToAdd)  
    {

        int slotnumber;
        for (slotnumber = 0; slotnumber < eqSlots.Length; slotnumber++)
        {
            
            if( eqSlots[slotnumber].belonging==itemToAdd.wearingSlot)
            {
                break;
            }

        }
        Image image = eqSlots[slotnumber].image;
        Armor itemToMove=Equipment.AddItem(rpg,itemToAdd);
        image.enabled = true;
        image.sprite = itemToAdd.appearance;
        image.raycastTarget = true;
        eqSlots[slotnumber].item = itemToAdd.noun;  
        if (itemToMove == null)
            return;
        AddInv(itemToMove);
        
    }
    public Item Unequip(string slot)
    {
        Item removedItem = Equipment.RemoveWithReturn(rpg, slot);
        return removedItem;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            wholeInv.SetActive(!wholeInv.activeSelf);
            equipmentWindow.SetActive(!equipmentWindow.activeSelf);
        }
    }
}
