﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
[System.Serializable]
public class EquipmentSlots : ScriptableObject
{

    [SerializeField]
    public string[] eqSlots;
 
}
