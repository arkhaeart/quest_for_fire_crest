﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Inventory : MonoBehaviour
{
    [HideInInspector]public static int maxInvSize = 8;
    public static Item[] NewInventory
    {
        get
        {
            Item[] inv = new Item[maxInvSize];
            return inv;
        }
    }
    public static Item[] InitInv()
    {
        return NewInventory;
    }
    public static Item[] InitInv (InventoryList list)
    {
        Item[] inv = NewInventory;
        
        for (int i = 0; i < list.inventory.Length; i++)
        {
            inv[i] = list.inventory[i];
        }
        return inv;
    }
    public static void RemoveItem(Item item,RPG rpg,int number)
    {
        int current = 0;
        int i = 0;
        foreach(Item it in rpg.inventory)
        {
            
            if(it==item)
            {
                if(it is Stackable)
                {
                    Stackable stack = (Stackable)it;
                    while(current<number)
                    {
                        if (rpg.parent.questReference == "player")
                        {
                            current++;
                            GameManager.plInvManager.RemoveInv(i, out bool empty);
                            if (empty)
                                break;
                        }
                        else
                        {
                            current++;
                            stack.ItemStack.Remove(out bool empty);
                            if (empty)
                                break;
                        }
                    }
                }
                else
                {
                    current++;
                    rpg.inventory[i] = null;
                }
            }
            if (current >= number)
                break;
            i++;
        }
        if(current<number)
        {

        }
    }
    public static void DropItem(Item item, GameObject parent)
    {
        GameObject instance = item.prefabRef;
        instance = Instantiate(instance, parent.transform.position.RandPos(),Quaternion.identity)as GameObject;
        instance.transform.SetParent(GameManager.unitMap.transform);
    }
    public static bool CheckItem(string ID,RPG rpg)
    {
        Item item = ItemDatabase.GetItem(ID);
        foreach(Item it in rpg.inventory)
        {
            if (it==item)
            {
                return true;
            }
        }
        if(item is Weapon)
        {
            if (item == rpg.weapon)
                return true;  
        }
        else if (item is Armor)
        {
            if (rpg.armorDict.ContainsValue(item as Armor))
            {
                return true;
            }
        }
        return false;
    }
    public static bool CheckItem(string ID, RPG rpg,int number)
    {
        Item item = ItemDatabase.GetItem(ID);
        if(GetItemCount(item,rpg.inventory)>=number)
            return true;
        return false;
    }
    public static int GetItemCount(string ID)
    {
        Item item= ItemDatabase.GetItem(ID);
        Item[] items = GameManager.player.GetComponent<Char>().rpg.inventory;
        int total = GetItemCount(item, items);
        return total;

    }
    public static int GetItemCount(Item item, Item[] items)
    {
        int total = 0;
        if(item is Stackable)
        {
            foreach(Item it in items)
            {
                Debug.Log(it);
                if (it == item)
                {
                    Stackable stack = it as Stackable;
                    total += stack.ItemStack.Count;
                    Debug.Log(stack.ItemStack.Count);
                }
            }
        }
        else if(item is Equipable)
        {
            foreach(Item it in items)
            {
                if (it == item)
                    total++;
            }
        }
        return total;
    }
}

