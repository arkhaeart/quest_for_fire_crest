﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentManager : MonoBehaviour
{

    public Equipment playerEquipment;
    public Image[] slots;
    public InvSlot[] invSlots;
}
