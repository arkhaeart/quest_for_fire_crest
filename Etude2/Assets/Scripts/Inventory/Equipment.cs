﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{

    public static Weapon AddItem( RPG rpg,Weapon itemToAdd)
    {
        Weapon itemToRemove = rpg.weapon;
        rpg.weapon = itemToAdd;

        return itemToRemove;
 
    }
    public static Armor AddItem( RPG rpg,Armor itemToAdd)
    {
        Armor itemToRemove = rpg.armorDict[itemToAdd.wearingSlot];
        rpg.armorDict[itemToAdd.wearingSlot] = itemToAdd;
        Debug.Log(rpg.armorDict[itemToAdd.wearingSlot].noun);
        return itemToRemove;
    }
    public static Item RemoveWithReturn(RPG rpg,string equipSlot)
    {
        Item itemToMove;
        switch(equipSlot)
        {
            case "weapon":
                itemToMove = rpg.weapon;
                rpg.weapon = null;
                break;
            default:
                itemToMove = rpg.armorDict[equipSlot];
                rpg.armorDict[equipSlot] = null;
                break;
        }
        return itemToMove;
    }
    public static Item[] RemoveWithReturnAll(RPG rpg)
    {
        Item[] itemsToMove = new Item[ItemDatabase.armorDict.Count + 1];
        itemsToMove[0] = RemoveWithReturn(rpg,"weapon");
        
        for (int i = 1; i < itemsToMove.Length; i++)
        {
            itemsToMove[i] = RemoveWithReturn(rpg, ItemDatabase.slotDict[i - 1]);
        }
        return itemsToMove;

    }
    public static ArmorProps GetArmorProps(RPG rpg)
    {
        ArmorProps[] armor = new ArmorProps[rpg.armorDict.Count];
        ArmorProps summary=new ArmorProps();
        for (int i = 0; i < armor.Length; i++)
        {
            Debug.Log(ItemDatabase.slotDict[i]);
            Debug.Log(rpg.armorDict[ItemDatabase.slotDict[i]]);
            if (rpg.armorDict[ItemDatabase.slotDict[i]] != null)
            {
                
                armor[i] = new ArmorProps(rpg.armorDict[ItemDatabase.slotDict[i]].armorTier);
                Debug.Log("hello there");
            }
            else
                armor[i] = new ArmorProps();
            
            summary.armorClass += armor[i].armorClass;
            summary.coverage += armor[i].coverage;
            summary.physicalDefense += armor[i].physicalDefense;

        }

        return summary;
    }
    public static Item[] GetAll(RPG rpg)
        {
        Item[] items = new Item[ItemDatabase.armorDict.Count + 1];
        items[0] = rpg.weapon;
        for (int i = 1; i < items.Length; i++)
        {
            items[i] = rpg.armorDict[ItemDatabase.slotDict[i-1]];
        }
        return items;
        }
}
