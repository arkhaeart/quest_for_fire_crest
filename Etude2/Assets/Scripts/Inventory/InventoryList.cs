﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="FireCrest/InvList")]
public class InventoryList : ScriptableObject
{
    public Item[] inventory;
    public InventoryList(Item[] _inventory)
    {
        inventory = _inventory;
    }
}
