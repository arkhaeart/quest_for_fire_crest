﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class InvManager : MonoBehaviour
{

    public InvSlot[] slots;
    public Text[] stackI;
   
    [HideInInspector] public Char player;
    [HideInInspector] public RPG rpg;
    [HideInInspector] public static float maxInvSize = 8;
    public GameObject wholeInv;

    protected virtual void Start()
    {

        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].image = slots[i].GetComponentsInChildren<Image>(true)[1];
            slots[i].slotNumber = i;
        }
    }
    public void AddInv(Item noun, params int[] index) 
    {
        Item itemToMove=null;
        rpg = player.rpg;
        for (int i = 0; i <= maxInvSize - 1; i++)
        {
            
            if (index.Length > 0)
                i = index[0];
            if (noun is Stackable)
            {
                Debug.Log(noun);
                Stackable item = (Stackable)noun;
                if (rpg.inventory[i]!=null)
                {
                    if (rpg.inventory[i].GetItem == noun.GetItem)
                    {
                        OnStackAdd<Stackable>(i);
                        break;
                    }
                    else
                        continue;
                }

                else
                {
                    OnStackCreate(item, i);
                    break;
                }
            }
            if (index.Length > 0)
            {
                itemToMove = RemoveInvWithReturn(index[0]);
            }
            if (rpg.inventory[i] != null)
                continue; 
            else
            {
                Debug.Log(i + " " + noun.noun);
                rpg.inventory[i] = noun;
                SlotProcess(i, noun);
                if (index.Length > 0&&itemToMove!=null)
                    AddInv(itemToMove);
                break;
            }
        }
    }
    void OnStackAdd<T>(int position) 
        where T: Stackable
    {
        Debug.Log("Added");
        T item = (T)rpg.inventory[position];
        item.ItemStack.Add(1);
        stackI[position].text = "x " + item.ItemStack.Count;
    }
    void OnStackCreate<T>(T noun,int position)
        where T: Stackable
    {
        if (noun is Stackable) 
        {
            noun.ItemStack=new StacK<Stackable>(noun);
            rpg.inventory[position] = noun;
            SlotProcess(position, noun);
            stackI[position].text = "x "+noun.ItemStack.Count;
        }
    }
    bool OnStackRemove<T>(int position)
        where T:Stackable
    {
        T item = (T)rpg.inventory[position];
        item.ItemStack.Remove(out bool empty);
        if(empty)
        {
            rpg.inventory[position] = null;
            stackI[position].text = null;
            SlotProcess(position, null);
            return true;
        }
        else
        {
            stackI[position].text = "x " + item.ItemStack.Count;
            return false;
        }
    }
    public void RemoveInv(int number)
    {
        if (rpg.inventory[number] != null)
        {
            if (rpg.inventory[number] is Stackable)
            {
                OnStackRemove<Stackable>(number);
            }
            else
            {
                rpg.inventory[number] = null;
                SlotProcess(number,null);
            }
        }
        else return;
    }
    public void RemoveInv(int number,out bool empty)
    {
        empty = OnStackRemove<Stackable>(number);
    }
    public Item RemoveInvWithReturn(int number)
    {
        if (rpg.inventory[number] != null)
        {
            SlotProcess(number, null);
            Item noun = rpg.inventory[number];
            rpg.inventory[number] = null;
            return noun;
        }
        else return null;
    }
    public void SlotProcess(int index,Item item)
    {
        Image image = slots[index].image;
        if (item != null)
        {
           image.enabled = true;
           image.sprite = item.appearance;
            slots[index].item = item.noun;
        }
        else
        {
            image.enabled = false;
            image.sprite = null;
            slots[index].item = null;
        }
    }
    public bool IsFull
    {
        get
        {
            for (int i = 0; i <= maxInvSize-1; i++)
            {
                if (player.rpg.inventory[i] == null)
                    return false;
            }
            return true;
        }
		    	
    }

}
