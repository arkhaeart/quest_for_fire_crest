﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StacK<T> :  IGetItem<Item> where T: Stackable
{
    T stackedItem;
    int itemCount=0;

    public StacK(T item)
    {
        stackedItem = item;
        itemCount = 1;
    }
    
    public int Count
    {
        get
        {
            return itemCount;
        }
    }
    public  Item GetItem
    {
        get
        {
            return stackedItem;
        }
    }
    public void Add(int number)
    {
        itemCount += number;
    }
    public T Remove(out bool empty)
    {
        itemCount--;
        if(itemCount==0)
        {
            empty = true;
        }
        else
            empty = false;
        return stackedItem;
    }
}
