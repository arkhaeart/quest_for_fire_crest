﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemUsage 
{
        static ConsumeEff currentEff=ConsumeEff.ZERO;
        static float currentval = 0;
        static string[] consLog;
public static string[] UseItem(string noun, Char unit)
    {
        RPG rpg = unit.rpg;
        Consumable item=null;
        consLog = new string[3];//first for mathf , second for value, third for param name

        
        for (int i = 0; i < unit.rpg.inventory.Length; i++)
        {
            if (unit.rpg.inventory[i] == null)
                continue;
            else if (unit.rpg.inventory[i].noun == noun)
            {
                item = (Consumable)unit.rpg.inventory[i];
                GameManager.plInvManager.RemoveInv(i);
                break;
            }
        }

        for (int i = 0; i < item.effects.Length; i++)
        {
            currentEff = item.effects[i].eff;
            currentval = item.effects[i].value;
            switch(item.effects[i].param)
            {
                case (ConsumeParam.HEALTH):
                    rpg.values.curHealth = OnConsumeEff(rpg.values.curHealth);
                    consLog[2] = "health ";
                    break;
                case (ConsumeParam.MAGIC):
                    rpg.values.curMagic= OnConsumeEff(rpg.values.curMagic);
                    consLog[2] = "magic ";
                    break;
                case (ConsumeParam.STAMINA):
                    rpg.values.curStamina = OnConsumeEff(rpg.values.curStamina);
                    consLog[2] = "stamina ";

                    break;
                case (ConsumeParam.SKILL):
                    break;
                case (ConsumeParam.STAT):
                    break;
            }
            
        }

        return consLog;
    }
    static int OnConsumeEff(float parameter)
    {

        switch(currentEff)
        {
            case (ConsumeEff.ZERO):
                parameter= 0;
                consLog[0] = "nullified ";
                consLog[1] = "all ";
                break;
            case (ConsumeEff.PLUS):
                parameter += currentval;
                consLog[0] = "restored ";
                consLog[1] = currentval.ToString();

                break;
            case (ConsumeEff.MULTUPLY):
                parameter *= currentval;
                consLog[0] = "restored ";
                consLog[1] = (currentval-parameter).ToString();
                break;
            case (ConsumeEff.MINUS):
                parameter -= currentval;
                consLog[0] = "depleted ";
                consLog[1] = currentval.ToString();
                break;
            case (ConsumeEff.MAX):
                parameter += 1000;
                break;
            case (ConsumeEff.DIVIDE):
                parameter /= currentval;
                consLog[0] = "depleted ";
                consLog[1] = (parameter-currentval).ToString();
                break;
                
        }
        return Mathf.RoundToInt(parameter);
    }

}
