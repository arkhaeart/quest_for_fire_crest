﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "FireCrest/Items/Weapon")]
public class Weapon : Equipable
{
    public int weight;
    public WeaponType weaponType;
    [Range(-100,100)] public int evasionBonus;
    [Range(-100, 100)] public int parryBonus;
    public RawDamage[] posAttacks;
    public override void GetItemDescription(out string[] info)
    {
        base.GetItemDescription(out info);
        info[2] = "Weapon parts: ";
        foreach (RawDamage raw in posAttacks)
        {
            info[2] = info[2] + " \n" + raw.weaponPart + " deals: " + raw.rawDamage+ " "+ raw.damageType.ToString()+" damage.";
        }

    }
}   
public enum WeaponType
{
    SMALL,
    ONEHANDED,
    TWOHANDED
}
public enum DamageType
{
    BLUNT,
    CUT,
    PIERCING,
    POISON,
    FIRE,
    ICE,
    MAGIC
}
