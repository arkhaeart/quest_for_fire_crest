﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Item : ScriptableObject,IGetItem<Item>
{

    public string noun = "Identidicator";
    public int basicValue;
    [TextArea]
    public string flDescr = "Full description";
    public Sprite appearance;
    public GameObject prefabRef;
    public virtual Item GetItem
    {
        get
        {
            return this;
        }
    }
    public virtual void GetItemDescription(out string[] info)
    {
        info = new string[5];
        info[0] = noun;
        info[1] = flDescr;
        info[4] = "Value: " + basicValue;
    }
    

    
}

 public interface IGetItem<T> 
{
    T GetItem
    {
        get;
    }
}