﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="FireCrest/Items/Armor")]
public class Armor : Equipable
{
    public string wearingSlot;
    public ArmorTier armorTier;
    public override void GetItemDescription(out string[] info)
    {
        base.GetItemDescription(out info);
        info[2] = wearingSlot;
        info[3] = armorTier.ToString();
    }

}
[System.Serializable]
public class ArmorProps
{
    public ArmorTier tier;
    public int armorClass;
    public int physicalDefense;
    public float coverage;
    public ArmorProps()
    {
    armorClass=0;
    physicalDefense=0;
    coverage=0;
}

    public ArmorProps(ArmorTier tier)
    {
        ArmorGrades grades = GameManager.itemDatabase.armorGrades;
        armorClass = grades.props[(int)tier].armorClass;
        physicalDefense = grades.props[(int)tier].physicalDefense;
        coverage = grades.props[(int)tier].coverage;
    }
}
public enum ArmorTier
{
    LEATHER,
    IRON,
    STEEL,
    MAGIC
}
