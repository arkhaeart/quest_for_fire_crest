﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FireCrest/Items/ArmorGrade")]
public class ArmorGrades : ScriptableObject
{
    public ArmorProps[] props;
}