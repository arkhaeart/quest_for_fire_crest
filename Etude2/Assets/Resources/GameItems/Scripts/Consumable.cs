﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="FireCrest/Items/Consumable")]
public class Consumable : Stackable
{
    public ConsumeEffect[] effects;

    public override void GetItemDescription(out string[] info)
    {
        base.GetItemDescription(out info);
        info[2] = "Effects: ";
        foreach(ConsumeEffect eff in effects)
        {
            info[2] = info[2] + "\n " + eff.eff.ToString() + " " + eff.value + " " + eff.param.ToString();
        }
    }
}
[System.Serializable]
public class ConsumeEffect
{
    public ConsumeEff eff;
    public ConsumeParam param;
    public float value;
    [HideInInspector] public string subparam;


}
public enum ConsumeEff
{
    MAX,
    ZERO,
    PLUS,
    MINUS,
    MULTUPLY,
    DIVIDE
}
public enum ConsumeParam
{
    HEALTH,
    MAGIC,
    STAMINA,
    STAT,
    SKILL
}