﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Stackable:Item
{
    [SerializeField]private int stackLimit=0;
    public int StackLimit
    {
        get
            {
                return stackLimit;
            }
    }
    StacK<Stackable> itemStack;
    public StacK<Stackable> ItemStack
    {
        get
        {
            return itemStack;
        }
        set
        {
            itemStack = value;   
        }
    }

}
