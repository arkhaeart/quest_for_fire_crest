﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="FireCrest/Items/Shield")]
public class Shield : Item
{
    public int coverage;
    public int defenceRate;
}
